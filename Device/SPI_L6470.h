/*
 *	SPI_L6470.h
 *
 *	Created on: 2015-04-18
 *	Author: 	Laurence DV
 *	Note:		This lib uses the SPI interface, the RESET pin, the FLAG pin and the BUSY pin. Not the STCK
 */

#ifndef SPI_L6470_
#define SPI_L6470_

/* -------------------------------- +
|	Include							|
+ ---------------------------------*/
#include <msp430.h>
//#include "driverlib.h"
#include <mxHal.h>
#include <Util/lhf.h>
#include <Include/mxStdDef.h>
#include <HardPeripheral/UsciSPI.h>

/* -------------------------------- +
|	Define							|
+ ---------------------------------*/
// == Project specific == //
#define L6470_SANITY_CHECK_ENABLE	1		//Define this to enable sanity check of almost all pointer and values
#define L6470_MAXIMUM_VS_RATIO		128		//This is the maximum allowable ratio of Vsupply the stepper motor is capable (thisValue/256 * Vsupply ie: 128 = 50% of Vsupply)
// ====================== //


// -- Operational constant -- //
#define L6470_CONST_TICK_NS			250

// -- Available SPI Command -- //
#define L6470_CMD_NOP				0
#define L6470_CMD_SETPARAM			0x00
#define L6470_CMD_GETPARAM			0x20
#define L6470_CMD_RUN				0x50
#define L6470_CMD_STEPCLOCK			0x58
#define L6470_CMD_MOVE				0x40
#define L6470_CMD_GOTO				0x60
#define L6470_CMD_GOTODIRECTION		0x68
#define L6470_CMD_GOUNTIL			0x82
#define L6470_CMD_RELEASESWITCH		0x92
#define L6470_CMD_GOHOME			0x70
#define L6470_CMD_GOMARK			0x78
#define L6470_CMD_RESETPOSITION		0xD8
#define L6470_CMD_RESETDEVICE		0xC0
#define L6470_CMD_SOFTSTOP			0xB0
#define L6470_CMD_HARDSTOP			0xB8
#define L6470_CMD_SOFTHIZ			0xA0
#define L6470_CMD_HARDHIZ			0xA8
#define L6470_CMD_GETSTATUS			0xD0

// -- Internal Register Address -- //
#define L6470_REG_ADD_ABSOLUTEPOSITION			0x01
#define L6470_REG_ADD_ELECTRICALPOSITION		0x02
#define L6470_REG_ADD_MARKPOSITION				0x03
#define L6470_REG_ADD_CURRENTSPEED				0x04
#define L6470_REG_ADD_ACCELERATION				0x05
#define L6470_REG_ADD_DECELERATION				0x06
#define L6470_REG_ADD_MAXSPEED					0x07
#define L6470_REG_ADD_MINSPEED					0x08
#define L6470_REG_ADD_FULLSTEPSPEED				0x15
#define L6470_REG_ADD_KVALHOLDING				0x09			// "K" multiplication factor of Vs when holding the motor
#define L6470_REG_ADD_KVALCONSTANTSPEED			0x0A			// "K" multiplication factor of Vs when running at constant speed
#define L6470_REG_ADD_KVALACCELERATION			0x0B			// "K" multiplication factor of Vs when accelerating
#define L6470_REG_ADD_KVALDECELERATION			0x0C			// "K" multiplication factor of Vs when decelerating
#define L6470_REG_ADD_INTERSECTSPEED			0x0D			// Speed at which the BEMF compensation curve changes
#define L6470_REG_ADD_STARTSLOPE				0x0E			// Compensation curve, when the speed is lower than the Intersect Speed
#define L6470_REG_ADD_ACCELERATIONFINALSLOPE	0x0F			// Compensation curve when accelerating and the speed is higher than the Intersect Speed
#define L6470_REG_ADD_DEVELERATIONFINALSLOPE	0x10			// Compensation curve when decelerating and the speed is higher than the Intersect Speed
#define L6470_REG_ADD_THERMALCOMPENSATION		0x11
#define L6470_REG_ADD_ADCOUTPUT					0x12
#define L6470_REG_ADD_OVERCURRENTTHRESHOLD		0x13
#define L6470_REG_ADD_STALLTHRESHOLD			0x14
#define L6470_REG_ADD_STEPMODE					0x16
#define L6470_REG_ADD_ALARMENABLE				0x17
#define L6470_REG_ADD_CONFIG					0x18
#define L6470_REG_ADD_STATUS					0x19

// -- Internal Register Size -- //
#define L6470_REG_SIZE_ABSOLUTEPOSITION			24			// Signed value with 22 valid bit
#define L6470_REG_SIZE_ELECTRICALPOSITION		9			// 2bit step, 7bit microstep
#define L6470_REG_SIZE_MARKPOSITION				24			// Signed value with 22 valid bit
#define L6470_REG_SIZE_CURRENTSPEED				20			// Fixed point in unsigned 0.20 format, lsb value is 2^-28
#define L6470_REG_SIZE_ACCELERATION				12			// Fixed point in unsigned 0.12 format, lsb value is 2^-40
#define L6470_REG_SIZE_DECELERATION				12			// Fixed point in unsigned 0.12 format, lsb value is 2^-40
#define L6470_REG_SIZE_MAXSPEED					10			// Fixed point in unsigned 0.10 format, lsb value is 2^-18
#define L6470_REG_SIZE_MINSPEED					13			// Fixed point in unsigned 0.12 format, lsb value is 2^-24 (BIT12 is a flag)
#define L6470_REG_SIZE_FULLSTEPSPEED			10			// Fixed point in unsigned 0.10 format, lsb value is 2^-18
#define L6470_REG_SIZE_KVALHOLDING				8			// 0 to 0.996
#define L6470_REG_SIZE_KVALCONSTANTSPEED		8			// 0 to 0.996
#define L6470_REG_SIZE_KVALACCELERATION			8			// 0 to 0.996
#define L6470_REG_SIZE_KVALDECELERATION			8			// 0 to 0.996
#define L6470_REG_SIZE_INTERSECTSPEED			14			// Fixed point in unsigned 0.14 format, lsb value is 2^-26 (0 to 0.000244125)
#define L6470_REG_SIZE_STARTSLOPE				8			// Fixed point in unsigned 0.8 format, lsb value is 2^-16 (0 to 0.004)
#define L6470_REG_SIZE_ACCELERATIONFINALSLOPE	8			// Fixed point in unsigned 0.8 format, lsb value is 2^-16 (0 to 0.004)
#define L6470_REG_SIZE_DEVELERATIONFINALSLOPE	8			// Fixed point in unsigned 0.8 format, lsb value is 2^-16 (0 to 0.004)
#define L6470_REG_SIZE_THERMALCOMPENSATION		4			// Fixed point in unsigned 0.4 format, lsb value is 2^-5 +1 (1 to 1.46875)
#define L6470_REG_SIZE_ADCOUTPUT				5
#define L6470_REG_SIZE_OVERCURRENTTHRESHOLD		4			// Unsigned 4bit, lsb value is 375mA (375 to 6000 mA)
#define L6470_REG_SIZE_STALLTHRESHOLD			7			// Unsigned 7bit, lsb value is 31.25mA (31.25 to 4000mA)
#define L6470_REG_SIZE_STEPMODE					8
#define L6470_REG_SIZE_ALARMENABLE				8
#define L6470_REG_SIZE_CONFIG					16
#define L6470_REG_SIZE_STATUS					16

#define L6470_SPI_BUF_SIZE			4		//Maximum command size is 4

/* -------------------------------- +
|	Type							|
+ ---------------------------------*/
typedef union {
	uint8_t all;
	struct {
		uint8_t syncEn:1;
		uint8_t syncSel:3;
		uint8_t :1;
		uint8_t stepSel:3;
	};
}L6470RegStepMode_t;

typedef union {
	uint8_t	all;
	struct {
		uint8_t	nonPerformedCommand:1;
		uint8_t	switchEvent:1;
		uint8_t	stallCoilB:1;
		uint8_t	stallCoilA:1;
		uint8_t	underVoltage:1;
		uint8_t thermalWarning:1;
		uint8_t thermalShutdown:1;
		uint8_t	overCurrent:1;
	};
}L6470RegAlarmEn_t;

typedef union {
	uint16_t all;
	struct {
		uint16_t	pwmFreqInteger:3;				// 0: 1 | 1: 2 | 2: 3 | 3: 4 | 4: 5 | 5: 6 | 6: 7 | 7: invalid
		uint16_t	pwmFreqDecimal:3;				// 0: 0.675 | 1: 0.750 | 2: 0.875 | 3: 1 | 4: 1.25 | 5: 1.5 | 6: 1.75 | 7: 2
		uint16_t	powerSlewRate:2;				// 0: 320 | 1: 75 | 2: 110 | 3: 260 V/us
		uint16_t	overCurrentAction:1;			// 0: nothing | 1: Bridge shutdown
		uint16_t	:1;
		uint16_t	voltageCompensationEnable:1;	// 0: disabled | 1: enabled
		uint16_t	switchAction:1;					// 0: hardStop interrupt | 1: simple user flag
		uint16_t	clockOutputEnable:1;			// 0: disabled | 1: enabled
		uint16_t	clockSource:3;					//refer to datasheet p.50
	};
}L6470RegConfig_t;

typedef union {
	uint16_t all;
	struct {
		uint16_t	stepClockMode:1;				// 1: in step-clock mode (STCK pin)
		uint16_t	stallCoilBFlag:1;				// 0: Stall detected on Coil B (cleared by GET_STATUS)
		uint16_t	stallCoilAFlag:1;				// 0: Stall detected on Coil A (cleared by GET_STATUS)
		uint16_t	overCurrentFlag:1;				// 0: Over current event detected (cleared by GET_STATUS)
		uint16_t	thermalShutdownFlag:1;			// 0: Thermal shutdown activated (cleared by GET_STATUS)
		uint16_t	thermalWarningFlag:1;			// 0: Thermal warning flagged (cleared by GET_STATUS)
		uint16_t	underVoltageLockOutFlag:1;		// 0: Under-voltage event detected (startup included) (cleared by GET_STATUS)
		uint16_t	invalidCommand:1;				// 1: Invalid command received on SPI interface	(cleared by GET_STATUS)
		uint16_t	nonPerformedCommandFlag:1;		// 1: Last command received cannot be performed (cleared by GET_STATUS)
		uint16_t	motorStatus:2;					// 0: stopped | 1: accelerating | 2: decelerating | 3: constant speed
		uint16_t	motorDirection:1;				// 0: reverse | 1: forward
		uint16_t	switchEventFlag:1;				// 1: Falling-edge detected (cleared by GET_STATUS)
		uint16_t	switchStatus:1;					// 0: switch opened | 1: switch closed
		uint16_t	busyFlag:1;						// 0: operation on-going | 1: L6470 idle (same as the BUSY pin)
		uint16_t	hiZ:1;							// 1: Bridges are currently in Hi-Z state
	};
}L6470RegStatus_t;

typedef struct {
	struct {
		stdState_t			state;
		volatile uint8_t *	resetPort;
		uint8_t				resetMask;
	}control;
	struct {
		UsciSPISlave_t * SPIslave;
		uint8_t 		rxBuf[L6470_SPI_BUF_SIZE];
		uint8_t 		txBuf[L6470_SPI_BUF_SIZE];
		stdState_t		SPIstate;
	}com;
	struct {
		int32_t				currentPosition;
		int16_t				electricalPosition;
		int32_t				markPosition;
		uint32_t			currentSpeed;
		uint16_t			accelerationSpeed;
		uint16_t			decelerationSpeed;
		uint16_t			maximumSpeed;
		uint16_t			minimumSpeed;
		uint8_t				kValHolding;
		uint8_t				kValConstantSpeed;
		uint8_t				kValAccelerating;
		uint8_t				kValDecelerating;
		uint16_t			intersectSpeed;
		uint8_t				startSlope;
		uint8_t				accelerationFinalSlope;
		uint8_t				decelerationFinalSlope;
		uint8_t				thermalCompensation;
		uint8_t				adcOut;
		uint8_t				overCurrentThreshold;
		uint8_t				stallThreshold;
		uint16_t			fullStepSpeed;
		L6470RegStepMode_t	stepMode;
		L6470RegAlarmEn_t	alarm;
		L6470RegConfig_t	config;
		L6470RegStatus_t	status;
	}reg;
}L6470object_t;

/* -------------------------------- +
|	Macro							|
+ ---------------------------------*/


/* -------------------------------- +
|	Prototype						|
+ ---------------------------------*/
// Create a L6470 object in memory and return it's handle
// You need as much object as you have physical L6470 connected
L6470object_t * L6470Create(UsciSPIModule_t * SPImodule, volatile uint8_t * SSport, uint8_t SSmask, volatile uint8_t * resetPort, uint8_t resetMask);

void L6470Configure(L6470object_t * handle);

void L6470Command(L6470object_t * handle);

void _L6470Parse(L6470object_t * handle);

void L6470Engine(L6470object_t * handle);

#endif /* SPI_L6470_ */
