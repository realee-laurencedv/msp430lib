/*
 *	SPI_L6470.c
 *
 *	Created on: 2015-04-18
 *	Author: Laurence DV
 */

#include "SPI_L6470.h"
/* -------------------------------- +
|	Global Variable					|
+ ---------------------------------*/
extern uint16_t heapAvailable;

/* -------------------------------- +
|	Function						|
+ ---------------------------------*/
L6470object_t * L6470Create(UsciSPIModule_t * SPImodule, volatile uint8_t * SSport, uint8_t SSmask, volatile uint8_t * resetPort, uint8_t resetMask)
{
	#ifdef L6470_SANITY_CHECK_ENABLE
		if (SPImodule <= NULL)
			return NULL;
		if (SSport <= NULL)
			return NULL;
		if (resetPort <= NULL)
			return NULL;
	#endif

	L6470object_t * tempPtr;

	// -- Create the object -- //
	tempPtr = (L6470object_t*)malloc(sizeof(L6470object_t));
	if (tempPtr <= NULL)
		return NULL;
	// ----------------------- //


	tempPtr->com.SPIslave = UsciSPISlaveCreate(SPImodule, SSport, SSmask);
	if (tempPtr->com.SPIslave == NULL)
	{
		free(tempPtr);
		return NULL;
	}

	heapAvailable -= sizeof(L6470object_t);
	tempPtr->control.resetPort = resetPort;
	tempPtr->control.resetMask = resetMask;
	tempPtr->control.state = stateInit;

	return tempPtr;
}

void L6470Destroy(L6470object_t * handle)
{

}

void L6470Configure(L6470object_t * handle)
{

}

void L6470Command(L6470object_t * handle)
{

}

void _L6470Parse(L6470object_t * handle)
{

}

void L6470Engine(L6470object_t * handle)
{
	#ifdef L6470_SANITY_CHECK_ENABLE
		if (handle <= NULL)
			return;
	#endif

	switch (handle->control.state)
	{
		case stateInit:
		{

			break;
		}
		default:	handle->control.state = stateInit;
	}
}
