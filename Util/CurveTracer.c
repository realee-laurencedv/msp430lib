/*
 * CurveTracer.c
 *
 *	Created on: 2015-03-13
 *	Author: Laurence DV
 */

#include "CurveTracer.h"
/* -------------------------------- +
|	Global Variable					|
+ ---------------------------------*/

/* -------------------------------- +
|	Internal helper function		|
+ ---------------------------------*/
void _CurveTracerTooSmall(uint8_t startVal, uint8_t endVal, uint8_t * userBuffer, uint8_t curveSize)
{
	if (curveSize == 0)
	{
		userBuffer[0] = endVal;
	}
	else if (curveSize == 1)
	{
		userBuffer[0] = startVal;
		userBuffer[1] = endVal;
	}
}

/* -------------------------------- +
|	Function						|
+ ---------------------------------*/
stdError_t CurveTracerLinear(uint8_t startVal, uint8_t endVal, uint8_t * userBuffer, uint8_t curveSize)
{
	if (userBuffer <= NULL)
		return errBadAddress;

	// -- Handle strait curve -- //
	if (startVal == endVal)
		CurveTracerConstant(startVal, userBuffer, curveSize);
	// ------------------------- //

	if (curveSize <= 1)
		_CurveTracerTooSmall(startVal, endVal, userBuffer, 0);
	else
	{

		uint8_t x;
		_iq runVal;
		_iq incrementVal;

		// -- compute the increment -- //
		runVal = _IQ(startVal);
		incrementVal = _IQdiv(_IQ(endVal) - _IQ(startVal), _IQ(curveSize));
		// --------------------------- //

		// -- Trace the actual curve -- //
		for(x=0; x<=curveSize; x++)
		{
			userBuffer[x] = _IQint(runVal + _IQ(0.5));	//Add 1 fractional msb to improve the rounding
			runVal += incrementVal;
		}
		// ---------------------------- //
	}
	return errSuccess;
}

stdError_t CurveTracerExponent(uint8_t param, uint8_t startVal, uint8_t endVal, uint8_t * userBuffer, uint8_t curveSize)
{
	if (userBuffer <= NULL)
		return errBadAddress;

	// -- Handle strait curve -- //
	if (startVal == endVal)
		CurveTracerConstant(startVal, userBuffer, curveSize);
	// ------------------------- //

	if (curveSize <= 1)
		_CurveTracerTooSmall(startVal, endVal, userBuffer, 0);
	else
	{
		uint8_t x;
		_iq internalVar;
		_iq paramQ = _IQ(param);

		// adjust internalVar to place the last point closest to "endVal" possible
		internalVar = _IQdiv( _IQlog(_IQ(endVal - startVal)) + paramQ , _IQ(curveSize));		//This need extreme amount of precision... would need IQ lib if ROM is available

		for (x=0; x<=curveSize; x++)
		{
			userBuffer[x] = startVal + _IQint(_IQexp(_IQrsmpy(internalVar,_IQ(x)) - paramQ) + _IQ(0.5));	//start + e^(internalVar*x - param)
		}
	}

	return errSuccess;
}

stdError_t CurveTracerHyperbola(uint8_t startVal, uint8_t endVal, uint8_t * userBuffer, uint8_t curveSize)
{
	if (userBuffer <= NULL)
		return errBadAddress;

	// -- Handle strait curve -- //
	if (startVal == endVal)
		CurveTracerConstant(startVal, userBuffer, curveSize);
	// ------------------------- //

	uint8_t x;
	_iq15 internalVar;

	if (curveSize <= 1)
		_CurveTracerTooSmall(startVal, endVal, userBuffer, curveSize);
	else
	{
		// adjust internalVar to plate the last point closest to "endVal" possible
		internalVar = _IQ15div( _IQ15(endVal - startVal) , _IQ15(curveSize * curveSize));

		for (x=0; x<=curveSize; x++)
		{
			userBuffer[x] = (uint8_t)_IQ15int((_IQ15rsmpy(internalVar , _IQ15(x*x)) + _IQ15(startVal)) + _IQ15(0.5));		//startVal + internalVar * x^2
		}
	}

	return errSuccess;
}

stdError_t CurveTracerConstant(uint8_t value, uint8_t * userBuffer, uint8_t curveSize)
{
	if (userBuffer <= NULL)
		return errBadAddress;

	uint16_t x;
	for (x=0; x<=curveSize; x++)
		userBuffer[x] = value;

	return errSuccess;
}
