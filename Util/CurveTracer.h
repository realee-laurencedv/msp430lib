/*
 * CurveTracer.h
 *
 *	Created on: 2015-03-13
 *	Author: Laurence DV
 *	Note:	This lib trace curve in designated user linear buffer
 *			It will output curve in octet but works in 16bit fix point precision
 *	Depend: QMathLib must be in the compiler include path (ex: "G:\Programs\TI\CCS\msp430\MSP430ware_1_97_00_47\iqmathlib\include")
 *			QMathLib must added to the project or added to the linker -l option (ex: "G:\Programs\TI\CCS\msp430\MSP430ware_1_97_00_47\iqmathlib\libraries\CCS\MPY32\5xx_6xx\QmathLib.a")
 */

#ifndef CURVETRACER_H_
#define CURVETRACER_H_
/*--------------------------------- +
|	Include							|
+ ---------------------------------*/
//#include "msp430.h"
#include <stddef.h>
#include <stdint.h>
#include <Util/lhf.h>
#include <Include/mxStdDef.h>
//#include "math.h"

#ifdef GLOBAL_Q
	#warning "Redefinition of GLOBAL_Q!"
#endif
#define GLOBAL_Q	7
#ifdef GLOBAL_IQ
	#warning "Redfinition of GLOBAL_IQ!"
#endif
#define GLOBAL_IQ	23
#include "QmathLib.h"
#include "IQmathLib.h"

/*--------------------------------- +
|	Define							|
+ ---------------------------------*/


/*--------------------------------- +
|	Type							|
+ ---------------------------------*/
typedef enum {
	linear = 0,
	exponent = 1,
	square = 2,
	logarithmic = 3,
	sinus = 4
}CurveTracerCurveType_t;

/*--------------------------------- +
|	Prototype						|
+ ---------------------------------*/
// Trace a linear curve in the userbuffer (a straight line)
// Formula:		point1 (0, startVal) to point2 (curveSize+1, endVal)
// Note:		curveSize is +1 internally, ie: 255 give you a size of 256
// Return:		errSuccess if successful,
//				errBadAddress if the userBuffer pointer is invalid
stdError_t CurveTracerLinear(uint8_t startVal, uint8_t endVal, uint8_t * userBuffer, uint8_t curveSize);

// Trace an exponent curve in the userbuffer
// Formula:		y = startVal + e^(internalVar*x - param)
// Note:		curveSize is +1 internally, ie: 255 give you a size of 256
//				internalVar is adjusted so when x=curveSize  y=endVal  (not entirely accurate)
// Return:		errSuccess if successful,
//				errBadAddress if the userBuffer pointer is invalid
stdError_t CurveTracerExponent(uint8_t param, uint8_t startVal, uint8_t endVal, uint8_t * userBuffer, uint8_t curveSize);

// Trace an hyperbola curve in the userbuffer
// Formula:		y = startVal + internalVar*x^2
// Note:		curveSize is +1 internally, ie: 255 give you a size of 256
//				internalVar is adjusted so when x=curveSize  y=endVal  (not entirely accurate)
// Return:		errSuccess if successful,
//				errBadAddress if the userBuffer pointer is invalid
stdError_t CurveTracerHyperbola(uint8_t startVal, uint8_t endVal, uint8_t * userBuffer, uint8_t curveSize);

// Trace a constant value in the userbuffer
// Formula:		y = value
// Note:		curveSize is +1 internally, ie: 255 give you a size of 256
// 				Mainly used to handle startVal == endValue in other tracer function
// Return:		errSuccess if successful,
//				errBadAddress if the userBuffer pointer is invalid
stdError_t CurveTracerConstant(uint8_t value, uint8_t * userBuffer, uint8_t curveSize);

#endif /* CURVETRACER_H_ */
