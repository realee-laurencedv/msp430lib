/*
 * lhf.h
 *
 *		Created on: 2015-02-19
 *		Author: Laurence DV
 *		Note:	DBG_PORT is assumed to be 8bit wide
 */

#ifndef LHF_H_
#define LHF_H_

/* ======================================== +
|	MAIN SWITCH								|
|	define this for using debug functions	|
+ =========================================*/
#define DEBUG_ENABLE		1

#if defined (__TMS470__)
/* ================================ +
|	TM4C TI compiler				|
+ =================================*/

/* -------------------------------- +
|	include							|
+ ---------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <inc/hw_memmap.h>
#include <inc/hw_types.h>
#include <inc/hw_gpio.h>
#include <driverlib/sysctl.h>
#define TARGET_IS_TM4C123_RB1
#include <driverlib/rom_map.h>

/* -------------------------------- +
|	Define							|
+ ---------------------------------*/
#define DBG_PORT_SYSCTL		(SYSCTL_PERIPH_GPIOD)
#define DBG_PORT			(GPIO_PORTD_AHB_BASE)
#define DBG_PORT_MASK		(0b11001111)			//Use to ignore some pin on the debug port
#define DBG_UART_PIN		(0b00000001)			//Transmit pin for software Debug Uart (on DBG_PORT)
#define DBG_BOOT_DELAY		100						//Cycle to wait after pulsing all debug pins to enable the uart

/*--------------------------------- +
|	Macro							|
+ ---------------------------------*/
#define SWAP(a, b) (((a) ^= (b)), ((b) ^= (a)), ((a) ^= (b)))

#define DBGPRINT(dbgCode)	DBGPRINT8(dbgCode)

#define DBGPRINT8(dbgCode)	(MAP_GPIOPinWrite(DBG_PORT, DBG_PORT_MASK, (uint8_t)dbgCode)),	\
							(MAP_GPIOPinWrite(DBG_PORT, DBG_PORT_MASK, 0))

#define DBGSET(x)			( MAP_GPIOPinWrite(DBG_PORT, x, 1) )
#define DBGCLR(x)			( MAP_GPIOPinWrite(DBG_PORT, x, 0) )
#define DBGTOG(x)			( MAP_GPIOPinWrite(DBG_PORT, x, MAP_GPIOPinRead(DBG_PORT,x)) )
#define DBGWRITE(x,val)		( MAP_GPIOPinWrite(DBG_PORT, x, val) )
#define DBGPULSE(x)			( (DBGSET(x)) , (DBGCLR(x)) )

#define _DBGUARTWRITE(val)	(DBGWRITE(DBG_UART_PIN, val))

/*--------------------------------- +
|	Prototype						|
+ ---------------------------------*/
void DBGInit(void);
void DBGUartSendByte(uint8_t byteToSend);
void DBGUartSendArray(uint8_t * arrayPtr, uint8_t arrayByteNb);
void DBGUartSendString(uint8_t * stringPtr);



#else defined (__MSP430__)
/* ================================ +
|	MSP430 TI compiler				|
+ =================================*/

/* -------------------------------- +
|	include							|
+ ---------------------------------*/
#include <stdint.h>
#include <stddef.h>
#include <msp430.h>

/* -------------------------------- +
|	Define							|
+ ---------------------------------*/
#define DBG_PORT			(P6OUT)
//#define DBG_UART_PIN		(BIT0)						//Transmit pin for software Debug Uart (on DBG_PORT)
#define DBG_BOOT_DELAY		100							//Cycle to wait after pulsing all debug pins to enable the uart

/*--------------------------------- +
|	Macro							|
+ ---------------------------------*/
#define SWAP(a, b) (((a) ^= (b)), ((b) ^= (a)), ((a) ^= (b)))

#define DBGPRINT(dbgCode)	DBGPRINT8(dbgCode)

#define DBGPRINT8(dbgCode)	((DBG_PORT = ((uint8_t)dbgCode)),					\
							(DBG_PORT = 0))
#define DBGPRINT16(dbgCode)	((DBG_PORT = (uint8_t)(((uint16_t)dbgCode)>>8)),	\
							(DBG_PORT = 0),										\
							(DBG_PORT = (uint8_t)((uint16_t)dbgCode)),			\
							(DBG_PORT = 0))


#define DBGPRINT32(dbgCode)	((DBG_PORT = (uint8_t)(((uint32_t)dbgCode)>>24)),	\
							(DBG_PORT = 0),										\
							(DBG_PORT = (uint8_t)(((uint32_t)dbgCode)>>16)),	\
							(DBG_PORT = 0),										\
							(DBG_PORT = (uint8_t)(((uint32_t)dbgCode)>>8)),		\
							(DBG_PORT = 0),										\
							(DBG_PORT = (uint8_t)((uint32_t)dbgCode)),			\
							(DBG_PORT = 0))

#define DBGSET(x)			(DBG_PORT |= ((uint8_t)x))
#define DBGCLR(x)			(DBG_PORT &= ~((uint8_t)x))
#define DBGTOG(x)			(DBG_PORT ^= ((uint8_t)x))
#define DBGPULSE(x)			(DBGTOG((uint8_t)x),								\
							DBGTOG((uint8_t)x))

#define _DBGUARTLOW			(DBGCLR(DBG_UART_PIN))
#define _DBGUARTHIGH		(DBGSET(DBG_UART_PIN))

/*--------------------------------- +
|	Prototype						|
+ ---------------------------------*/
void DBGInit(void);
void DBGUartSendByte(uint8_t byteToSend);
void DBGUartSendArray(uint8_t * arrayPtr, uint8_t arrayByteNb);
void DBGUartSendString(uint8_t * stringPtr);

#endif

#endif /* LHF_H_ */
