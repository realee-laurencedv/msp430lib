/*
 * lfh.c
 *
 *		Created on: 2015-05-21
 *		Author: Laurence DV
 */

#include "lhf.h"

/* ================================ +
|	Private Globals					|
+ =================================*/
#if defined DEBUG_ENABLE
//DO NOT USE THOSE VARIABLES ANYWHERE ELSE!!!
uint8_t __gwu8 = 0;					//Global Work Unsigned 8bit
uint16_t __gwu16 = 0;				//Global Work Unsigned 16bit
uint32_t __gwu32 = 0;				//Global Work Unsigned 32bit

#if defined DBG_UART_PIN
uint8_t	__dbgMaskUart = 0;
const uint8_t __ErrDefaultMsg[] = {'E','R','R','O','R'};
#endif

#endif


#if defined (__TMS470__)
/* ================================ +
|	TM4C TI compiler				|
+ =================================*/

void DBGInit(void)
{
#ifdef DEBUG_ENABLE
	MAP_SysCtlPeripheralEnable(DBG_PORT_SYSCTL);
	MAP_GPIOPinTypeGPIOOutput(DBG_PORT, DBG_PORT_MASK);

	// Test all debug pin //
	uint16_t i = 0x01;
	for (; i<0x100; i<<=1)
		DBGPULSE(i);
	// ------------------ //

	// Start the uart //
	#if defined DBG_UART_PIN
		for (i=DBG_BOOT_DELAY; i!=0; i--)
			__no_operation();
		_DBGUARTWRITE(1);
	#endif
	// -------------- //
#endif
}

#if defined DBG_UART_PIN
	void DBGUartSendByte(uint8_t byteToSend)
	{
	#ifdef DEBUG_ENABLE
		int8_t i;

		uint32_t intState;
		intState = MAP_IntMasterDisable();

		//Start bit
		_DBGUARTWRITE(0);
		_nop();
		_nop();
		_nop();
		_nop();
		_nop();
		_nop();
		_nop();
		_nop();

		//Data
		for(i=0; i<8 ;i++)
		{
			_DBGUARTWRITE(byteToSend & 0x01);
			byteToSend >>= 1;
		}

		//Stop bit
		_DBGUARTWRITE(1);
		_nop();
		_nop();
		_nop();
		_nop();
		_nop();
		_nop();
		_nop();
		_nop();

		if (intState)
			MAP_IntMasterEnable();
	#endif
	}

	void DBGUartSendArray(uint8_t * arrayPtr, uint8_t arrayByteNb)
	{
	#ifdef DEBUG_ENABLE
		uint8_t i;
		for (i=0; i< arrayByteNb; i++)
			DBGUartSendByte(arrayPtr[i]);
	#endif
	}

	void DBGUartSendString(uint8_t * stringPtr)
	{
	#ifdef DEBUG_ENABLE
		while((*stringPtr) != NULL)
		{
			DBGUartSendByte(*stringPtr);
			stringPtr++;
		}
	#endif
	}
#endif




#elif defined (__MSP430__)
/* ================================ +
|	MSP430 TI compiler				|
+ =================================*/
void DBGInit(void)
{
#if defined DEBUG_ENABLE
	// Test all debug pin //
	uint16_t i = 0x01;
	for (; i<0x100; i<<=1)
		DBGPULSE(i);
	// ------------------ //

	// Start the uart //
#if defined DBG_UART_PIN
	for (i=DBG_BOOT_DELAY; i!=0; i--)
		__no_operation();
	_DBGUARTHIGH;
#endif
	// -------------- //
#endif
}

void DBGIOPrint8(uint8_t valueToPrint)
{
#if defined DEBUG_ENABLE
	__gwu8 = DBG_PORT;
	DBGPRINT8(valueToPrint);
	DBG_PORT = __gwu8;
#endif
}

void DBGIOPrint16(uint16_t valueToPrint)
{
#if defined DEBUG_ENABLE
	__gwu16 = DBG_PORT;
	DBGPRINT16(valueToPrint);
	DBG_PORT = __gwu16;
#endif
}

void DBGIOPrint32(uint32_t valueToPrint)
{
#if defined DEBUG_ENABLE
	__gwu32 = DBG_PORT;
	DBGPRINT32(valueToPrint);
	DBG_PORT = __gwu32;
#endif
}

void DBGUartSendByte(uint8_t byteToSend)
{
#if defined DEBUG_ENABLE && defined DBG_UART_PIN
	int8_t i = 0;
	__dbgMaskUart = 0x1;

	uint8_t intState;
	intState = __get_interrupt_state();
	__disable_interrupt();

	//Start bit
	_DBGUARTLOW;
	__no_operation();
	__no_operation();
	__no_operation();
	__no_operation();
	__no_operation();
	__no_operation();
	__no_operation();
	__no_operation();

	//Data
	for(; i<8 ;i++)
	{
		if (byteToSend & __dbgMaskUart)
		{
			_DBGUARTHIGH;
			__no_operation();
			__no_operation();
		}
		else
		{
			_DBGUARTLOW;
		}
		__dbgMaskUart <<= 1;
	}
	__no_operation();
	__no_operation();

	//Stop bit
	_DBGUARTHIGH;
	__no_operation();
	__no_operation();
	__no_operation();
	__no_operation();
	__no_operation();
	__no_operation();
	__no_operation();
	__no_operation();

	__set_interrupt_state(intState);
#endif
}

void DBGUartSendArray(uint8_t * arrayPtr, uint8_t arrayByteNb)
{
#if defined DEBUG_ENABLE && defined DBG_UART_PIN
	uint8_t i;
	for (i=0; i< arrayByteNb; i++)
		DBGUartSendByte(arrayPtr[i]);
#endif
}

void DBGUartSendString(uint8_t * stringPtr)
{
#if defined DEBUG_ENABLE && defined DBG_UART_PIN
	while((*stringPtr) != NULL)
	{
		DBGUartSendByte(*stringPtr);
		stringPtr++;
	}
#endif
}

#else
#warning "Compiler not LHF-compatible"

#endif
