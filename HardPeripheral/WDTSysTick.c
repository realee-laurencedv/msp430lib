/*
 *	WDTSysTick.c
 *
 *	Created on:	2015-03-25
 *	Author:		Laurence DV
 */

#include "HardPeripheral/WDTSysTick.h"
/* -------------------------------- +
|	Global Variable					|
+ ---------------------------------*/
struct {
	uint32_t period;
	uint16_t sysTick;
	struct {
		waiter_t queue[WDTSYSTICK_QUEUE_SIZE];
		waiter_t * current;
		uint8_t pendingNb;
	}wait;
}WDTSysTick = {
	.period = 0,
	.sysTick = 0,
	.wait.queue = "",
	.wait.current = NULL,
	.wait.pendingNb = 0
};

const float wdtSysTickPrescaler[] = {2147483648, 134217728, 8388608, 524288, 32768, 8192, 512, 64};


/* -------------------------------- +
|	Function						|
+ ---------------------------------*/
waiter_t * _wdtSysTickCreateWaiter(void)
{
	int8_t i;
	/* Find the first available memory slot */
	for (i=(WDTSYSTICK_QUEUE_SIZE-1); i>=0 ; i--)
	{
		if ((WDTSysTick.wait.queue[i]).next == WDTSYSTICK_UNASSIGNED)
		{
			WDTSysTick.wait.pendingNb++;
			return (&WDTSysTick.wait.queue[i]);
		}
	}
	return NULL;
}


// ==== Management function ==== //
void wdtSysTickInit(void)
{
	// Stop the watchdog
	HWREG16(WDT_A_BASE + OFS_WDTCTL) = 	WDTSYSTICK_REG_PASSWORD + WDTSYSTICK_TIMER_STOPPED;	// stop the Watchdog timer

	// -- Init the queue -- //
	uint8_t i;
	for (i=0; i<WDTSYSTICK_QUEUE_SIZE; i++)
		WDTSysTick.wait.queue[i].next = WDTSYSTICK_UNASSIGNED;		//All items are unassigned
	// -------------------- //

	//Enable interrupt
	SFRIE1 |= WDTIE;
}

uint32_t wdtSysTickPeriodSet(uint32_t tickPeriodus, uint8_t clockSource)
{
	float clockFreq;		//in Hz
	float realPeriod;		//in us
	int8_t divider;

	// Stop the watchdog
	HWREG16(WDT_A_BASE + OFS_WDTCTL) = 	WDTSYSTICK_REG_PASSWORD + WDTSYSTICK_TIMER_STOPPED;	// stop the Watchdog timer

	// -- Compute the closer tick period -- //
	switch (clockSource)
	{
		case WDTSYSTICK_CLKSRC_SMCLK:	clockFreq = (float)UCS_getSMCLK();	break;
		case WDTSYSTICK_CLKSRC_ACLK:	clockFreq = (float)UCS_getACLK();	break;
		case WDTSYSTICK_CLKSRC_VLOCK:	clockFreq = 10000.0;				break;
		case WDTSYSTICK_CLKSRC_XCLK:	clockFreq = 10000.0;				//TODO: set the real clock from X_CLK (not present in F5529)
	}

	for (divider=7; divider>0; divider--)
	{
		realPeriod = (1000000.0) / (clockFreq / wdtSysTickPrescaler[divider]);
		if (realPeriod >= (float)tickPeriodus)
			break;
	}
	// ------------------------------------ //

	// -- Initialize the timer -- //
	WDTSysTick.sysTick = 0;
	WDTSysTick.period = (uint32_t)realPeriod;
	HWREG16(WDT_A_BASE + OFS_WDTCTL) = 	WDTSYSTICK_REG_PASSWORD +
										WDTSYSTICK_CLEAR_TIMER +
										WDTSYSTICK_TIMER_RUNNING +
										WDTSYSTICK_MODE_INTERVAL +
										clockSource + divider;
	// -------------------------- //

	// -- Compute the real period -- //
	return WDTSysTick.period;
	// ----------------------------- //
}

uint32_t wdtSysTickPeriodGet(void)
{
	return WDTSysTick.period;
}

uint32_t wdtSysTickMaxWaitGet_us(void)
{
	return WDTSysTick.period * UINT16_MAX;
}

uint16_t wdtSysTickTimeStamp(void)
{
	return WDTSysTick.sysTick;
}

uint16_t wdtSysTickConvertusToSysTick(uint32_t us)
{
	uint16_t sysTickValue = 0;

	// -- Compute the sysTick value -- //
	sysTickValue = (uint16_t)((float)us / (float)WDTSysTick.period);
	if (sysTickValue == 0)
		sysTickValue = 1;
	// ------------------------------- //

	return sysTickValue;
}

// ==== Queue function ==== //
stdError_t wdtSysTickWait(uint16_t waitTick, stdState_t * flag)
{
	if (flag == NULL)
		return errBadAddress;

	uint16_t intState = __get_interrupt_state();
	__disable_interrupt();

	if (WDTSysTick.wait.pendingNb >= WDTSYSTICK_QUEUE_SIZE)
	{
		__set_interrupt_state(intState);
		return errNoSpace;
	}
	else
	{
		*flag = stateWait;						// Tell the user to wait

		/* -- Compute the targetTick tick -- */
		if (waitTick == 0)
			waitTick = 1;
		/* --------------------------------- */

		// -- Create the waiter -- //
		waiter_t * newWaiter = _wdtSysTickCreateWaiter();
		newWaiter->targetTick = WDTSysTick.sysTick + waitTick;
		newWaiter->userFlag = flag;
		newWaiter->next = newWaiter;			//Default to the end of the list
		// ----------------------- //

		// -- Place it at the correct place in the array -- //
		if (WDTSysTick.wait.pendingNb == 1)		//It's the first waiter
		{
			WDTSysTick.wait.current = newWaiter;
		}
		else
		{
			waiter_t * previous = NULL;
			waiter_t * current = WDTSysTick.wait.current;

			/* While the current waiter is waiting for less than the new one */
			while ((current->targetTick - WDTSysTick.sysTick) < (waitTick))
			{
				/* Still inside the list, check the next item */
				if (current->next != current)
				{
					previous = current;
					current = current->next;
				}
				/* This was the last element, append it there */
				else
				{
					current->next = newWaiter;
					__set_interrupt_state(intState);
					return errSuccess;
				}
			}

			/* This is now the first element of the list */
			if (previous == NULL)
			{
				WDTSysTick.wait.current = newWaiter;
			}
			else
			{
				previous->next = newWaiter;				//Put it at the correct place in the line
			}
			newWaiter->next = current;					//Place the next one after the new item
		}
		// ------------------------------------------------ //
	}
	__set_interrupt_state(intState);
	return errSuccess;
}

stdError_t wdtSysTickWait_us(uint32_t waitus, stdState_t * flag)
{
	return wdtSysTickWait(wdtSysTickConvertusToSysTick(waitus),flag);
}

stdError_t wdtSysTickWaitAbort(stdState_t * flag)
{
	uint16_t intState = __get_interrupt_state();
	__disable_interrupt();

	if (WDTSysTick.wait.pendingNb)
	{
		waiter_t * previous = NULL;
		waiter_t * current = WDTSysTick.wait.current;

		// Find the waiter
		while (current->userFlag != flag)
		{
			/* is there a next element? */
			if (current->next != current)
			{
				previous = current;
				current = current->next;
			}
			/* No exit with error */
			else
			{
				__set_interrupt_state(intState);
				return errNotFound;
			}
		}

		//Reconnect the next element to the previous one
		if (current->next != current)						//Check if we are at the end of the list
		{
			if (previous == NULL)
			{
				WDTSysTick.wait.current = current->next;	//This is now the first in line
			}
			else
			{
				previous->next = current->next;				//Reconnect
			}
		}
		else
		{
			previous->next = previous;						/* The previous element is not the last one */
		}

		WDTSysTick.wait.pendingNb--;
		*(current->userFlag) = stateAbort;					//Tell the user we aborted
		current->next = WDTSYSTICK_UNASSIGNED;				//This waiter is now available
	}
	__set_interrupt_state(intState);
	return errSuccess;
}

stdError_t wdtSysTickWaitUpdate_us(uint32_t newValue, stdState_t * flag)
{
	return wdtSysTickWaitUpdate(wdtSysTickConvertusToSysTick(newValue), flag);
}

stdError_t wdtSysTickWaitUpdate(uint16_t newValue, stdState_t * flag)
{
	stdError_t returnVal = errSuccess;
	uint16_t intState = __get_interrupt_state();
	__disable_interrupt();

	wdtSysTickWaitAbort(flag);						/* Cancel the current timeout */
	returnVal = wdtSysTickWait(newValue, flag);		/* Place a new delay event if we didn't found it previously */

	__set_interrupt_state(intState);
	return returnVal;
}

// ==== Interrupt Service Routine ==== //
void wdtSysTickISR(void)
{
	WDTSysTick.sysTick++;

	if (WDTSysTick.wait.pendingNb)					/* This shield the comparaison from not-cleared-but-empty waiter */
	{
		while(WDTSysTick.sysTick == WDTSysTick.wait.current->targetTick)
		{
			*(WDTSysTick.wait.current->userFlag) = stateDone;				/* Tell the user time as elapsed */

			WDTSysTick.wait.pendingNb--;
			if (WDTSysTick.wait.current->next != WDTSysTick.wait.current)
			{
				waiter_t * previous = WDTSysTick.wait.current;
				WDTSysTick.wait.current = WDTSysTick.wait.current->next;	/* Select the next item in line */
				previous->next = WDTSYSTICK_UNASSIGNED;						/* This waiter memory space is free now */
			}
			else
			{
				WDTSysTick.wait.current->next = WDTSYSTICK_UNASSIGNED;
				break;
			}
		}
	}
}
