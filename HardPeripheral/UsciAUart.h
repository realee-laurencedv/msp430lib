/*
 * UsciAUart.h
 *
 *	Created on: 2015-02-26
 *	Author: Laurence DV
 *	Note:	All function except ISR are not safe to call in interrupt context
 */

#ifndef USCIAUART_H_
#define USCIAUART_H_

/* -------------------------------- +
|	Include							|
+ ---------------------------------*/
#include <msp430.h>
#include <driverlib.h>
#include <mxHal.h>
#include <Util/lhf.h>
#include <Include/mxStdDef.h>
#include <SoftPeripheral/RingBuffer.h>

/* -------------------------------- +
|	Define							|
+ ---------------------------------*/
#define USCIAUART_MODULE_NB	2

// ==== Init Parameters ==== //
// CTL0
#define USCIAUART_CTL0_PARITY_NO		0x00
#define USCIAUART_CTL0_PARITY_ODD		0x80
#define USCIAUART_CTL0_PARITY_EVEN		0xC0
#define USCIAUART_CTL0_FIRST_MSB		0x20
#define USCIAUART_CTL0_FIRST_LSB		0x00
#define USCIAUART_CTL0_SIZE_8BIT		0x00
#define USCIAUART_CTL0_SIZE_7BIT		0x10
#define USCIAUART_CTL0_STOP_1BIT		0x00
#define USCIAUART_CTL0_STOP_2BIT		0x08
#define USCIAUART_CTL0_MODE_UART		0x00
#define USCIAUART_CTL0_MODE_IDLELINE	0x02
#define USCIAUART_CTL0_MODE_ADDRESS		0x04
#define USCIAUART_CTL0_MODE_AUTOBAUD	0x06

// CTL1
#define USCIAUART_CTL1_CLOCK_USCICLK	0x00
#define USCIAUART_CTL1_CLOCK_ACLK		0x40
#define USCIAUART_CTL1_CLOCK_SMCLK		0x80
#define USCIAUART_CTL1_DORM_OFF			0x00
#define USCIAUART_CTL1_DORM_ON			0x08

// MCTL
#define USCIAUART_MCTL_OVERSAMPLING_ON	0x01
#define USCIAUART_MCTL_OVERSAMPLING_OFF	0x00
#define USCIAUART_MCTL_Builder(brf,brs,overEn)	((((uint8_t)brf & 0xF) <<4) + (((uint8_t)brf & 0x7)<<1) + (overEn & 0x1))
// ========================= //

#define	_USCIAUART_RX_INT	2
#define	_USCIAUART_TX_INT	4

#define _USCIAUART_UCAxCTL0_MASK		0xFE
#define _USCIAUART_UCAxCTL1_MASK		0xC8

/* -------------------------------- +
|	Type							|
+ ---------------------------------*/
typedef struct{
	uint16_t	baseAddress;
	rBuf_t *	rxBuffer;
	rBuf_t *	txBuffer;
	stdState_t	state;
}UsciAUartHandle_t;

typedef union{
	uint8_t all;
	struct {
		uint8_t :1;
		uint8_t operatingMode:2;
		uint8_t stopSize:1;
		uint8_t dataSize:1;
		uint8_t bitOrder:1;
		uint8_t parity:2;
	};
}UsciAUartDataFormat_t;		//You can apply this to CTL0 directly

/* -------------------------------- +
|	Prototype						|
+ ---------------------------------*/
// Initialize the USCI A library run it before any other function
void UsciAUartLibInit(void);

// Create a USCI A module as a UART and return it's handle
// Return NULL if there was no more module initializable
UsciAUartHandle_t * UsciAUartCreate(uint16_t baseAddress, uint16_t bufferSize);

// Destroy a USCI A module configuration and return it to it's default state
// Return:	errSuccess if successful,
//			-errBadAddress if the handle is invalid
stdError_t UsciAUartDestroy(UsciAUartHandle_t * moduleHandle);

// Configure a USCI without destroying the buffer
// For register initialization refer to the "init parameter" section
// UCAxBRW is a uint16_t for the baudrate generator prescaler
// Return:	errSuccess if sucessful,
//			-errNoDevice if the handle is invalid,
//			-errNotReady if the module is not in a known state
stdError_t UsciAUartConfigure(UsciAUartHandle_t * moduleHandle, uint8_t UCAxCTL0, uint8_t UCAxCTL1, uint16_t UCAxBRW, uint8_t UCAxMCTL);

// Resize the buffer of the Usci
stdError_t UsciAUartBufferResize(UsciAUartHandle_t * moduleHandle, uint16_t newSize);

// Return the current size of the buffers of the USCI
#define UsciAUartBufferSizeGet(moduleHandle)		((moduleHandle)->rxBuffer->bufferSize)

// Completly empty the buffer of it's current content
void UsciAUartBufferFlush(UsciAUartHandle_t * moduleHandle);

// Return true if the USCI is currently sending or receiving data
#define UsciAUartIsBusy(moduleHandle)				((HWREG8(((moduleHandle)->baseAddress) + OFS_UCAxSTAT) & UCBUSY))

// Send $byteNb bytes from $srcPtr with the $moduleHandle USCI
// Return the number of byte buffered
uint16_t UsciAUartSend(UsciAUartHandle_t * moduleHandle, uint8_t * srcPtr, uint16_t byteNb);

// Pull from the internal buffer $byteNb bytes and put them in $dstPtr from the $moduleHandle USCI
// Return the number of byte transfered
uint16_t UsciAUartReceive(UsciAUartHandle_t * moduleHandle, uint8_t * dstPtr, uint16_t byteNb);

// Return the number of byte received currently waiting in the buffer
//#define UsciAUartRxPendingByteGet(moduleHandle)		((moduleHandle)->rxBuffer->elementNb)
uint16_t UsciAUartRxPendingByteGet(UsciAUartHandle_t * moduleHandle);

// Return the number of byte queued for sending, but not yet sent
#define UsciAUartTxPendingByteGet(moduleHandle)		((moduleHandle)->txBuffer->elementNb)

// Return true if the buffer is full
#define UsciAUartTxBufferIsFull(moduleHandle)		(rBufBufferIsFull((moduleHandle)->txBuffer))
#define UsciAUartRxBufferIsFull(moduleHandle)		(rBufBufferIsFull((moduleHandle)->rxBuffer))

// Return the state of the USCI module
#define UsciAUartStateGet(moduleHandle)				((moduleHandle)->state)

// Set the state of the USCI module
// Be sure to know what you are doing when using this macro-function
#define UsciAUartStateSet(moduleHandle, newState)	((moduleHandle)->state = newState)

// Interrupt service routine for USCI module in UART mode
// Warning no pointer sanity check
void UsciAUartISR(UsciAUartHandle_t * moduleHandle);

#endif /* USCIAUART_H_ */
