/*
 * GPIO.h
 *
 *	Created on: 2015-03-25
 *	Author: Laurence DV
 */

#ifndef GPIO_H_
#define GPIO_H_

/* -------------------------------- +
|	Include							|
+ ---------------------------------*/
#include <msp430.h>
//#include "driverlib.h"
#include <mxHal.h>
#include <Util/lhf.h>
#include <Include/mxStdDef.h>

/* -------------------------------- +
|	Define							|
+ ---------------------------------*/


/* -------------------------------- +
|	Type							|
+ ---------------------------------*/


/* -------------------------------- +
|	Macro							|
+ ---------------------------------*/
#define	GPIOsetDirection(port, pins, direction)		(HWREG8((port)+OFS_PxDIR) = )

/* -------------------------------- +
|	Prototype						|
+ ---------------------------------*/


#endif /* GPIO_H_ */
