/*
 * UsciBI2C.c
 *
 *	Created on: 2015-04-24
 *	Author:		Laurence DV
 *	TODO:		Use Memory bloc for dynamic tranfer Structure allocation
 */

#include "UsciBI2C.h"
/* -------------------------------- +
|	Global Variable					|
+ ---------------------------------*/
struct{

}UsciBI2Clib;

/* -------------------------------- +
|	Function						|
+ ---------------------------------*/
void UsciI2CLibInit(void)
{

}

// ==== Management Function ==== //
UsciBI2CModule_t * UsciBI2CCreate(uint16_t baseAddress, uint8_t transferQueueSize)
{
	if (baseAddress <= 0)
		return NULL;

	if (transferQueueSize == 0)
		transferQueueSize = _USCIBI2C_DEFAULT_TRANSFERQUEUE_SIZE;

	UsciBI2CModule_t * tempPtr;

	// -- Allocate the module -- //
	tempPtr = (UsciBI2CModule_t*)malloc(sizeof(UsciBI2CModule_t));
	if (tempPtr <= NULL)
		return NULL;
	// ------------------------- //

	// -- Create transfer queue -- //
	tempPtr->transferQueue = rBufCreate(sizeof(UsciBI2CTransfer_t *) * transferQueueSize);
	if (tempPtr->transferQueue == NULL)
	{
		free(tempPtr);
		return NULL;
	}
	// --------------------------- //

	// -- Save information -- //
	tempPtr->baseAddress = baseAddress;
	tempPtr->state = stateInit;
	// ---------------------- //

	return tempPtr;
}

void UsciBI2CDestroy(UsciBI2CModule_t * moduleHandle)
{
	if (moduleHandle <= NULL)
		return;
	//TODO
	// Find any pending transfer for this module
	// Stop everything

	rBufDestroy(moduleHandle->transferQueue);
	free(moduleHandle);
}

stdError_t UsciBI2CConfigure(UsciBI2CModule_t * moduleHandle, uint8_t UCBxCTL0Val, uint8_t UCBxCTL1Val, uint16_t UCBxBRxVal)
{
	if (moduleHandle <= NULL)
		return errNoDevice;

	// -- Configure the USCI -- //
	HWREG8(moduleHandle->baseAddress + OFS_UCBxCTL1) |= UCSWRST;				//Disable the USCI
	HWREG8(moduleHandle->baseAddress + OFS_UCBxCTL0) = (UCBxCTL0Val & _USCIBI2C_UCBxCTL0_MASK) | UCSYNC;
	HWREG8(moduleHandle->baseAddress + OFS_UCBxCTL1) = (UCBxCTL1Val & _USCIBI2C_UCBxCTL1_MASK) | UCSWRST;
	HWREG8(moduleHandle->baseAddress + OFS_UCBxBRW) = UCBxBRxVal;
	HWREG8(moduleHandle->baseAddress + OFS_UCBxCTL1) &= ~UCSWRST;				//Enable the USCI
	// ------------------------ //

	moduleHandle->state = stateIdle;

	return errSuccess;
}

// ==== Master Function ==== //
UsciBI2CSlave_t * UsciBI2CSlaveCreate(UsciBI2CModule_t * moduleHandle, uint16_t slaveAddress)
{
#ifdef USCIBI2C_SANITY_CHECK_ENABLE
	if (moduleHandle <= NULL)
		return NULL;
#endif
	UsciBI2CSlave_t * tempPtr;

	// -- Allocate memory -- //
	tempPtr = (UsciBI2CSlave_t*)malloc(sizeof(UsciBI2CSlave_t));
	if (tempPtr != NULL)
	{
		tempPtr->state = stateReady;
		tempPtr->module = moduleHandle;
		tempPtr->address = slaveAddress;
	}
	// --------------------- //
	return tempPtr;
}

void UsciBI2CSlaveDestroy(UsciBI2CSlave_t * slaveHandle)
{
	if (slaveHandle <= NULL)
		return;
	//TODO
	// Find any pending transfer for this slave

	free(slaveHandle);
}

// ==== Transfer Function ==== //
stdError_t UsciBI2CTransferStart(UsciBI2CTransfer_t * transferHandle)
{
	if (transferHandle <= NULL)
		return errBadAddress;

	UsciBI2CModule_t * tempModule = transferHandle->slaveHandle->module;

	// -- Start immediatly -- //
	if (tempModule->state == stateIdle)
	{
		//if (transferHandle->stateFlag == )			//TODO: dafuq is that!?
		// -- Configure the module -- //
		HWREG8(tempModule->baseAddress + OFS_UCBxI2CSA) = transferHandle->slaveHandle->address;		// Configure the Slave address
		tempModule->currentTransfer = transferHandle;
		tempModule->state = stateActive;
		tempModule->dataPtr = transferHandle->dataPtr;
		// -------------------------- //

		// -- Configure the internal FSM -- //
		tempModule->FSMstep[0] = I2C_address;
		tempModule->FSMstepID = 0;
		switch(transferHandle->transferType.all)
		{
			case I2C_S_Ad_Rx_P:
			{
				tempModule->FSMstep[1] = I2C_receive;
				tempModule->dataPtrLimitRx = transferHandle->dataPtr + transferHandle->byteNbRx;
				tempModule->FSMlastStep = 1;
				_USCIBI2C_EN_RECEIVER(tempModule);
				break;
			}
			case I2C_S_Ad_Tx_P:
			{
				tempModule->FSMstep[1] = I2C_transmit;
				tempModule->dataPtrLimitTx = transferHandle->dataPtr + transferHandle->byteNbTx;
				tempModule->FSMlastStep = 1;
				_USCIBI2C_EN_TRANSMITTER(tempModule);
				break;
			}
			case I2C_S_Ad_Tx_S_Ad_Rx_P:
			{
				tempModule->FSMstep[1] = I2C_transmit;
				tempModule->FSMstep[2] = I2C_start;
				tempModule->FSMstep[3] = I2C_address;
				tempModule->FSMstep[4] = I2C_receive;
				tempModule->dataPtrLimitTx = transferHandle->dataPtr + transferHandle->byteNbTx;
				tempModule->dataPtrLimitRx = tempModule->dataPtrLimitTx + transferHandle->byteNbRx;
				tempModule->FSMlastStep = 4;
				_USCIBI2C_EN_TRANSMITTER(tempModule);
				break;
			}
			case I2C_S_Ad_Tx_P_S_Ad_Rx_P:
			{
				tempModule->FSMstep[1] = I2C_transmit;
				tempModule->FSMstep[2] = I2C_stop;
				tempModule->FSMstep[3] = I2C_start;
				tempModule->FSMstep[4] = I2C_address;
				tempModule->FSMstep[5] = I2C_receive;
				tempModule->dataPtrLimitTx = transferHandle->dataPtr + transferHandle->byteNbTx;
				tempModule->dataPtrLimitRx = tempModule->dataPtrLimitTx + transferHandle->byteNbRx;
				tempModule->FSMlastStep = 5;
				_USCIBI2C_EN_TRANSMITTER(tempModule);
			}
		}
		// -------------------------------- //

		// -- Start the Hardware -- //
		_USCIBI2C_DO_START(tempModule);
		_USCIBI2C_INTERRUPT_ENABLE(tempModule, USCIBI2C_INT_RX|USCIBI2C_INT_TX|USCIBI2C_INT_NAK);	//Wait to the Slave address to be sent
		// ------------------------ //
	}
	// -- Append to queue --- //
	else
	{
		uint16_t tempAddress = (uint16_t)transferHandle;
		if (sizeof(UsciBI2CTransfer_t *) != rBufArrayPush(tempModule->transferQueue, (uint8_t *)(&tempAddress), sizeof(UsciBI2CTransfer_t *)))
			return errTryAgain;
	}
	// ---------------------- //

	return errSuccess;
}

stdError_t UsciBI2CTransferAbort(UsciBI2CTransfer_t * transferHandle)
{
	if (transferHandle > NULL)
	{
		transferHandle->stateFlag = I2C_abort;
		return errSuccess;
	}
	return errBadAddress;
}

// ==== Interrupt Service Routine ==== //
void UsciBI2CISR(UsciBI2CModule_t * moduleHandle)
{
	UsciBI2CTransfer_t * transfer = moduleHandle->currentTransfer;

	uint16_t baseAddress = moduleHandle->baseAddress;
	uint8_t intStatus = HWREG8(baseAddress + OFS_UCBxIFG);
	uint8_t loopFlag;
	uint8_t stopFlag = 0;

	do {
		loopFlag = 0;
		switch (moduleHandle->FSMstep[moduleHandle->FSMstepID])
		{
			case I2C_start:
			{
				break;
			}
			case I2C_stop:
			{
				break;
			}
			case I2C_address:
			{
				//* -- Slave didn't answer -- *//
				if (intStatus & USCIBI2C_INT_NAK)
				{
					transfer->stateFlag = I2C_nak;	//No slave present
					stopFlag = 1;
				}
				//* -- Slave answered ------- *//
				else
				{
					moduleHandle->FSMstepID++;
					loopFlag = 1;					//Loop to do the next step
				}
				//* ------------------------- *//

				break;
			}
			case I2C_transmit:
			{
				if (intStatus & USCIBI2C_INT_NAK)
				{
					transfer->stateFlag = I2C_abort;	//Error while transmitting
					stopFlag = 1;
				}
				else
				{
					if (moduleHandle->dataPtr == moduleHandle->dataPtrLimitTx)
					{
						moduleHandle->FSMstepID++;
						loopFlag = 1;					//Loop to do the next step
					}
					else if (intStatus & USCIBI2C_INT_TX)
					{
						HWREG8(baseAddress + OFS_UCBxTXBUF) = *(moduleHandle->dataPtr);		//load a byte
						moduleHandle->dataPtr++;
					}
				}
				break;
			}
			case I2C_receive:
			{
				if (moduleHandle->dataPtr == moduleHandle->dataPtrLimitRx)
				{
					moduleHandle->FSMstepID++;
					loopFlag = 1;					//Loop to do the next step
				}
				else if (intStatus & USCIBI2C_INT_RX)
				{
					*(moduleHandle->dataPtr) = HWREG8(baseAddress + OFS_UCBxTXBUF);			//save a byte
					moduleHandle->dataPtr++;
				}
			}
		}

		// -- Catch the last step -- //
		if (moduleHandle->FSMstepID > moduleHandle->FSMlastStep)
		{
			transfer->stateFlag = I2C_done;
			stopFlag = 1;
			loopFlag = 0;
		}
		// ------------------------- //
	} while (loopFlag);

	// -- Close the transfer -- //
	if (stopFlag)
	{
		_USCIBI2C_DO_STOP(moduleHandle);								//Do a Stop on the bus
		_USCIBI2C_INTERRUPT_DISABLE(moduleHandle, USCIBI2C_INT_ALL);	//Disable all int
		HWREG8(baseAddress + OFS_UCBxIFG) &= USCIBI2C_INT_ALL;			//Clear all flag
	}
	// ------------------------ //
}
