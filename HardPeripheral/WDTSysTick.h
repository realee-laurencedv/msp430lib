/*
 *	WDTSysTick.h
 *
 *	Created on:	2015-03-25
 *	Author:		Laurence DV
 *	Need:		driverlib UCS lib to know the frequency of different clock source
 *	Note:		Uses single precision floating point division function
 *				ATTENTION: It can miss the target sysTick if you use us function and the period is largeit is too short!!!
 */

#ifndef WDTSYSTICK_H_
#define WDTSYSTICK_H_

/* -------------------------------- +
|	Include							|
+ ---------------------------------*/
#include <msp430.h>
#include <stdint.h>
#include <driverlib.h>			//need UCS function to know the real freq of clocks
//#include <mxHal.h>
#include <Util/lhf.h>
#include <Include/mxStdDef.h>

/* -------------------------------- +
|	Define							|
+ ---------------------------------*/
#define WDTSYSTICK_QUEUE_SIZE		16

// -- Init parameter -- //
#define WDTSYSTICK_CLKSRC_SMCLK		0x00		//Default
#define WDTSYSTICK_CLKSRC_ACLK		0x20
#define WDTSYSTICK_CLKSRC_VLOCK		0x40
#define WDTSYSTICK_CLKSRC_XCLK		0x60
// -------------------- //


// -- Internal parameter -- //
#define WDTSYSTICK_REG_PASSWORD		0x5A
#define WDTSYSTICK_TIMER_STOPPED	0x80
#define WDTSYSTICK_TIMER_RUNNING	0x00		//Default
#define WDTSYSTICK_MODE_WATCHDOG	0x00		//Default
#define WDTSYSTICK_MODE_INTERVAL	0x10
#define WDTSYSTICK_CLEAR_TIMER		0x08
#define WDTSYSTICK_DIV_64			0x07
#define WDTSYSTICK_DIV_512			0x06
#define WDTSYSTICK_DIV_8192			0x05
#define WDTSYSTICK_DIV_32768		0x04		//Default
#define WDTSYSTICK_DIV_524288		0x03
#define WDTSYSTICK_DIV_8388608		0x02
#define WDTSYSTICK_DIV_134217728	0x01
#define WDTSYSTICK_DIV_2147483648	0x00

// -- Internal define -- //
#define WDTSYSTICK_TIMER_MAX_VALUE	(0xFFFFFFFF)
#define WDTSYSTICK_REG_PASSWORD		(WDTPW)
#define WDTSYSTICK_UNASSIGNED		(NULL)

/* -------------------------------- +
|	Type							|
+ ---------------------------------*/
typedef struct waiter_t_struct{
	uint16_t					targetTick;
	stdState_t *				userFlag;
	struct waiter_t_struct *	next;
}waiter_t;

/* -------------------------------- +
|	Macro							|
+ ---------------------------------*/



/* -------------------------------- +
|	Prototype						|
+ ---------------------------------*/
// Initialize the interval mode and stop the watchdod mode
// This should be the first thing you execute after PUC
void wdtSysTickInit(void);

// Compute the desired tick period and start the timer
// It will try to give AT LEAST the designated tick period, but without control of the clock source
// it can't guarantee the exactitude of this period
// Param:		tickPeriodus	desired tick period (in us)
// 				clockSource		desired clock source (see "Init parameter" define section)
// Return:		the actual tick period (in us)
uint32_t wdtSysTickPeriodSet(uint32_t tickPeriodus, uint8_t clockSource);

// Return the current real sysTick period in us
uint32_t wdtSysTickPeriodGet(void);

// Return the maximum delay you can make with the current sysTick period value
uint32_t wdtSysTickMaxWaitGet_us(void);

// Return the current tick value
// Useful for debouncing and cooperative multitask delay
uint16_t wdtSysTickTimeStamp(void);

// Convert a number of micro-seconds into a number of sysTick
// Note:	This function uses float
//			It does not handle overflow, you must be intelligent and not use a value that would overflow the sysTick 16bit value
//			Use wdtSysTickMaxWaitGetus if you must check for overflow
uint16_t wdtSysTickConvertusToSysTick(uint32_t us);

// Tell the sysTick system to trigger the flag after the specified amount of tick
// Param:		waitTick		number of tick to wait before flagging
//				flag			pointer to the flag (init at stateWait, set to stateGo when the wait is done)
// Return:		errorCode		errSuccess if successful
//								errNoSpace if the Queue is full
//								errBadAddress if $flag is invalid
stdError_t wdtSysTickWait(uint16_t waitTick, stdState_t * flag);

// Same thing as wdtSysTickWait but in micro-seconds instead of in sysTick
// Note:	This function uses float
stdError_t wdtSysTickWait_us(uint32_t waitus, stdState_t * flag);

// Cancel a previously queued "waiter" and remove it from the list
// Note:		This function will cancel the first waiter it find, as it would be useless to have two waiter on the same flag
// Param:		flag			pointer to the flag previously given to the wdtSysTick
// Return:		errSuccess if successful
//				errNotFound if we did not found a corresponding waiter
stdError_t wdtSysTickWaitAbort(stdState_t * flag);

// Update an active waiter with a new value in sysTick
// Note:		This disable global interrupt for the time we update the value
//				The new value will be computed from the moment we call this function, not the original one
stdError_t wdtSysTickWaitUpdate(uint16_t newValue, stdState_t * flag);

// Update an active waiter with a new value in micro-seconds
// Note:		make the us to sysTick conversion before disabling global interrupt for the time we update the sysTick value
stdError_t wdtSysTickWaitUpdate_us(uint32_t newValue, stdState_t * flag);

void wdtSysTickISR(void);

#endif /* WDTSYSTICK_H_ */
