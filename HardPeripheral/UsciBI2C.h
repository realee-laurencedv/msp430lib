/*
 * UsciBI2C.h
 *
 *	Created on: 2015-04-24
 *	Author:		Laurence DV
 *	Note:		This lib only support single Master Mode 7bit addressing
 *				During transfer the data in the user's buffer need to stay valid until the UsciBI2CframeStep_t flag is set to (I2C_nak, I2C_abort or I2C_done)
 */
#ifndef USCIBI2C_H_
#define USCIBI2C_H_

/* -------------------------------- +
|	Include							|
+ ---------------------------------*/
#include <msp430.h>
//#include "driverlib.h"
#include <mxHal.h>
#include <Util/lhf.h>
#include <Include/mxStdDef.h>
#include <SoftPeripheral/RingBuffer.h>

/* -------------------------------- +
|	Define							|
+ ---------------------------------*/
#define USCIBI2C_SANITY_CHECK_ENABLE			1		//Define this to enable sanity check of almost all pointer and values (no check in ISR)
#define USCIBI2C_OPERATION_MAXNB				6

// ==== Init Parameter ==== //
#define USCIBI2C_CTL0_OWNADDRESS_7BIT			0x00
#define USCIBI2C_CTL0_OWNADDRESS_10BIT			0x80
#define USCIBI2C_CTL0_SLAVEADDRESS_7BIT			0x00
#define USCIBI2C_CTL0_SLAVEADDRESS_10BIT		0x40
#define USCIBI2C_CTL0_MULTIMASTER_OFF			0x00
#define USCIBI2C_CTL0_MULTIMASTER_ON			0x20
#define USCIBI2C_CTL0_ROLE_MASTER				0x08
#define USCIBI2C_CTL0_ROLE_SLAVE				0x00
#define USCIBI2C_CTL0_MODE_I2C					0x06

#define USCIBI2C_CTL1_CLOCK_UCLKI				0x00
#define USCIBI2C_CTL1_CLOCK_ACLK				0x40
#define USCIBI2C_CTL1_CLOCK_SMCLK				0x80
// ======================== //

// ---- Interrupt ---- //
#define USCIBI2C_INT_NAK						0x20
#define USCIBI2C_INT_ARBITRATIONLOST			0x10
#define USCIBI2C_INT_STOPDETECT					0x08	//Use only in slave mode (maybe in MM)
#define USCIBI2C_INT_STARTDETECT				0x04	//Use only in slave mode (maybe in MM)
#define USCIBI2C_INT_TX							0x02
#define USCIBI2C_INT_RX							0x01
#define USCIBI2C_INT_ALL						0x3F

#define USCIBI2C_IV_ARBITRATIONLOST				0x02
#define USCIBI2C_IV_NAK							0x04
#define USCIBI2C_IV_STARTDETECT					0x06
#define USCIBI2C_IV_STOPDETECT					0x08
#define USCIBI2C_IV_RX							0x0A
#define USCIBI2C_IV_TX							0x0C
// ------------------- //

// -- Control -- //
#define USCIBI2C_CTL1_DIR_RECEIVER				0x00
#define USCIBI2C_CTL1_DIR_TRANSMITTER			0x10
#define USCIBI2C_CTL1_GENERATE_NAK				0x08
#define USCIBI2C_CTL1_GENERATE_STOP				0x04
#define USCIBI2C_CTL1_GENERATE_START			0x02

#define USCIBI2C_I2COA_GENERALCALL_RESPOND		0x8000
#define USCIBI2C_I2COA_GENERALCALL_IGNORE		0x0000
// ------------- //

// ---- Internal use ---- //
#define _USCIBI2C_DEFAULT_TRANSFERQUEUE_SIZE	4

#define	_USCIBI2C_FSM_MAX_STEP					6

#define _USCIBI2C_UCBxCTL0_MASK					0b11101110
#define _USCIBI2C_UCBxCTL1_MASK					0b11000000


/* -------------------------------- +
|	Macro							|
+ ---------------------------------*/
#define _USCIBI2C_INTERRUPT_DISABLE(module, x)			(HWREG8((module)->baseAddress + OFS_UCBxIE) &= ~(x))
#define _USCIBI2C_INTERRUPT_ENABLE(module, x)			(HWREG8((module)->baseAddress + OFS_UCBxIE) |= (x))

#define _USCIBI2C_EN_TRANSMITTER(module)				(HWREG8((module)->baseAddress + OFS_UCBxCTL1) |= USCIBI2C_CTL1_DIR_TRANSMITTER)
#define _USCIBI2C_EN_RECEIVER(module)					(HWREG8((module)->baseAddress + OFS_UCBxCTL1) &= ~USCIBI2C_CTL1_DIR_TRANSMITTER)

#define _USCIBI2C_DO_STOP(module)						(HWREG8((module)->baseAddress + OFS_UCBxCTL1) |= USCIBI2C_CTL1_GENERATE_STOP)
#define _USCIBI2C_DO_START(module)						(HWREG8((module)->baseAddress + OFS_UCBxCTL1) |= USCIBI2C_CTL1_GENERATE_START)
#define _USCIBI2C_DO_NAK(module)						(HWREG8((module)->baseAddress + OFS_UCBxCTL1) |= USCIBI2C_CTL1_GENERATE_NAK)

/* -------------------------------- +
|	Type							|
+ ---------------------------------*/
typedef union {
	enum {
		I2C_S_Ad_Rx_P =				0b00000000,		//Start, Slave Address, Receive, Stop
		I2C_S_Ad_Tx_P =				0b00100000,		//Start, Slave Address, Transmit, Stop
		I2C_S_Ad_Tx_S_Ad_Rx_P =		0b00111000,		//Start, Slave Address, Transmit, Start, Receive, Stop
		I2C_S_Ad_Tx_P_S_Ad_Rx_P =	0b00110000		//Start, Slave Address, Transmit, Stop, Start, Receive, Stop
	}all;
	struct {
		uint8_t stop:1;				//0: do a Stop
		uint8_t op2dir:1;			//0: selfRx | 1: selfTx
		uint8_t op2add:1;			//0: send address
		uint8_t	transition:1;		//0: Stop then Start | 1: ReStart
		uint8_t	opNb:1;				//0: 1 operation | 1: 2 operation
		uint8_t	op1dir:1;			//0: selfRx | 1: selfTx
		uint8_t	op1add:1;			//0: send address
		uint8_t	start:1;			//0: do a Start
	};
}UsciBI2CTransferType_t;

typedef enum {
	I2C_start,
	I2C_address,
	I2C_transmit,
	I2C_receive,
	I2C_stop,
	I2C_done = stateDone,
	I2C_abort = stateAbort,
	I2C_nak
}UsciBI2CframeStep_t;

typedef struct _UsciBI2CModuleStruct {
	stdState_t							state;
	uint16_t							baseAddress;
	rBuf_t *							transferQueue;
	struct _UsciBI2CTransferStruct *	currentTransfer;
	uint8_t								byteCntTx;
	uint8_t								byteCntRx;
	uint8_t	*							dataPtr;
	uint8_t *							dataPtrLimitTx;
	uint8_t *							dataPtrLimitRx;
	UsciBI2CframeStep_t					FSMstep[_USCIBI2C_FSM_MAX_STEP];
	uint8_t								FSMstepID;
	uint8_t								FSMlastStep;
}UsciBI2CModule_t;

typedef struct _UsciBI2CSlaveStruct {
	stdState_t						state;
	uint16_t						address;	//I2C address of the slave
	struct _UsciBI2CModuleStruct *	module;		//USCI module to which this slave is attached
}UsciBI2CSlave_t;

typedef struct _UsciBI2CTransferStruct {
	UsciBI2CTransferType_t				transferType;
	struct _UsciBI2CSlaveStruct	*		slaveHandle;
	uint8_t								byteNbTx;
	uint8_t								byteNbRx;
	uint8_t *							dataPtr;
	UsciBI2CframeStep_t 				stateFlag;
}UsciBI2CTransfer_t;


/* -------------------------------- +
|	Prototype						|
+ ---------------------------------*/
void UsciI2CLibInit(void);

UsciBI2CModule_t * UsciBI2CCreate(uint16_t baseAddress, uint8_t transferQueueSize);
void UsciBI2CDestroy(UsciBI2CModule_t * moduleHandle);
stdError_t UsciBI2CConfigure(UsciBI2CModule_t * moduleHandle, uint8_t UCBxCTL0Val, uint8_t UCBxCTL1Val, uint16_t UCBxBRxVal);

UsciBI2CSlave_t * UsciBI2CSlaveCreate(UsciBI2CModule_t * moduleHandle, uint16_t slaveAddress);
void UsciBI2CSlaveDestroy(UsciBI2CSlave_t * slaveHandle);

stdError_t UsciBI2CTransferStart(UsciBI2CTransfer_t * transferHandle);
stdError_t UsciBI2CTransferAbort(UsciBI2CTransfer_t * transferHandle);

void UsciBI2CISR(UsciBI2CModule_t * moduleHandle);
#endif /* USCIBI2C_H_ */
