/*
 * UsiSPI.h
 *
 *	Created on: 2015-04-11
 *	Author: Laurence DV
 *	Note:	USCI A & B are extremely similar in SPI mode, everything specific to the "A" type is also valid the "B" type.
 */

#ifndef USCISPI_H_
#define USCISPI_H_

/* -------------------------------- +
|	Include							|
+ ---------------------------------*/
#include <msp430.h>
//#include "driverlib.h"
#include <mxHal.h>
#include <Util/lhf.h>
#include <Include/mxStdDef.h>
#include <SoftPeripheral/RingBuffer.h>

/* -------------------------------- +
|	Define							|
+ ---------------------------------*/
#define USCISPI_MODULE_NB		4							//TODO: remove the need for this define (not portable...)

#define USCISPI_SANITY_CHECK_ENABLE					1		//Define this to enable sanity check of almost all pointer and values (no check in ISR)

// ==== Init Parameter ==== //
#define USCISPI_CTL0_CLKPHASE_WRITE_THEN_READ		0x00
#define USCISPI_CTL0_CLKPHASE_READ_THEN_WRITE		0x80
#define USCISPI_CTL0_CLKPOLARITY_IDLE_LOW			0x00
#define USCISPI_CTL0_CLKPOLARITY_IDLE_HIGH			0x40
#define USCISPI_CTL0_FIRST_LSB						0x00
#define USCISPI_CTL0_FIRST_MSB						0x20
#define USCISPI_CTL0_SIZE_8BIT						0x00
#define USCISPI_CTL0_SIZE_7BIT						0x10
#define USCISPI_CTL0_ROLE_SLAVE						0x00
#define USCISPI_CTL0_ROLE_MASTER					0x08
#define USCISPI_CTL0_MODE_3PINSPI					0x00
#define USCISPI_CTL0_MODE_4PINSPI_STE_ACTIVE_HIGH	0x02
#define USCISPI_CTL0_MODE_4PINSPI_STE_ACTIVE_LOW	0x04

#define USCISPI_CTL1_CLOCK_ACLK						0x40
#define USCISPI_CTL1_CLOCK_SMCLK					0x80

#define USCISPI_STAT_LISTEN_ON						0x80
#define USCISPI_STAT_LISTEN_OFF						0x00
// ======================== //

#define	_USCISPI_RX_INT_MASK						1
#define	_USCISPI_TX_INT_MASK						2

#define _USCISPI_UCxxCTL0_MASK						0xFE
#define _USCISPI_UCxxCTL1_MASK						0xC0

#define _USCISPI_DEFAULT_TRANSFERQUEUE_SIZE			4

#define _USCISPI_TRANSFERID_CNT_INIT				1
#define _USCISPI_TRANSFERID_CNT_ROLLOVER			0xFFFF

/* -------------------------------- +
|	Type							|
+ ---------------------------------*/

typedef union {
	uint8_t			all;
	struct{
		uint8_t					toggleDelay:4;		//Number of cycle to wait when toggling SS (this number is *5 internally +35 cycle ie 4 = (4*5+35) = 55)
		uint8_t					toggleSS:1;			//Toggle SS after each byte
		uint8_t					:3;
	};
}UsciSPITransferMode_t;

typedef	struct _usciSpiTransferStruct {
		uint16_t						transferID;			//semi-unique id of this transfer
		struct _usciSpiSlaveStruct *	slaveHandle;
		uint16_t						byteNb;				//Total byte number of the transfer
		uint8_t *						rxBuffer;			//Pointer to the receive buffer
		uint8_t *						txBuffer;			//Pointer to the transmit buffer
		UsciSPITransferMode_t			mode;
		stdState_t *					stateFlag;			//Pointer to a flag that will be set to stateDone when the transfer is done
}UsciSPITransfer_t;

typedef struct _usciSpiModuleStruct {
	stdState_t					state;
	uint16_t					baseAddress;
	struct {
		UsciSPITransfer_t	transfer;
		uint16_t			byteTx;
		uint16_t			byteRx;
	}current;
	rBuf_t *					transferQueue;
	uint8_t						transferQueueSize;	//Size of the transfer Queue
	uint16_t **					transferAbortID;	//Array for abort request
	uint8_t						transferAbortNb;
}UsciSPIModule_t;

typedef struct _usciSpiSlaveStruct {
	stdState_t			state;				//Current state of this slave
	UsciSPIModule_t * 	module;				//USCI module to which this slave is attached
	volatile uint8_t *	SSPort;				//HW reg of the port containing the CS pin
	uint8_t				SSpinMask;			//Mask to apply to the HW reg to select the Slave
}UsciSPISlave_t;

/* -------------------------------- +
|	Macro							|
+ ---------------------------------*/


/* -------------------------------- +
|	Prototype						|
+ ---------------------------------*/
// ==== Management Function ==== //
// Initialize the USCI SPI library run it before any other function
void UsciSPILibInit(void);

// Create a USCI A or B module as a SPI master and return it's handle
// Return NULL if there was an error (no more memory, bad address, etc)
UsciSPIModule_t * UsciSPICreate(uint16_t baseAddress, uint8_t transferQueueSize);

// Destroy a USCI A or B module configuration and return it to it's default state
// Return:	errSuccess if successful,
//			errBadAddress if the handle is invalid
void UsciSPIDestroy(UsciSPIModule_t * moduleHandle);

// Configure a USCI with the provided parameters
// For register initialization refer to the "init parameter" section
// UCAxBRW is a uint16_t for the baudrate generator prescaler
// Return:	errSuccess if sucessful,
//			errNoDevice if the handle is invalid,
stdError_t UsciSPIConfigure(UsciSPIModule_t * moduleHandle, uint8_t UCxxCTL0Val, uint8_t UCxxCTL1Val, uint16_t UCxxBRxVal);

// Main FSM to correctly run the SPI driver
// Note: should be executed frequently
void UsciSPIEngine(UsciSPIModule_t * moduleHandle);


// ==== Master Function ==== //
// Create a new slave control structure and return it's handle
UsciSPISlave_t * UsciSPISlaveCreate(UsciSPIModule_t * moduleHandle, volatile uint8_t * SSPort, uint8_t SSpinMask);

// Destroy a slave control structure from memory
void UsciSPISlaveDestroy(UsciSPISlave_t * slaveHandle);


// ==== Transfer Function ==== //
// Queue a transfer on the specified slave using the specified buffers
// Note: it will start the transfer as soon as the module is ready
// Return a unique transfer id used to abort it (0 is invalid and signify an error) (roll-over after 65535)
uint16_t UsciSPITransferStart(UsciSPISlave_t * slaveHandle, uint16_t byteNb, uint8_t * rxBuffer, uint8_t * txBuffer, stdState_t * stateFlag, UsciSPITransferMode_t transferMode);

// Abort a currently queued (or active) transfer
// Return:		errSuccess if successful
stdError_t UsciSPITransferAbort(uint16_t transferID);


// ==== Interrupt Service Routine ==== //
// Interrupt service routine for USCI module in SPI mode
// Warning no pointer sanity check
void UsciSPIISR(UsciSPIModule_t * moduleHandle);

#endif /* USCI_H_ */
