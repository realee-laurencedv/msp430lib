/*
 * UsciAUart.c
 *
 *  Created on: 2015-02-26
 *      Author: Laurence DV
 */

#include "HardPeripheral/UsciAUart.h"
/* -------------------------------- +
|	Global Variable					|
+ ---------------------------------*/
const uint16_t _UsciAUartValidBaseAddress[USCIAUART_MODULE_NB] = {USCI_A0_BASE, USCI_A1_BASE};

struct{
	stdState_t moduleState[USCIAUART_MODULE_NB];
	UsciAUartHandle_t * module[USCIAUART_MODULE_NB];
	uint8_t moduleInitNb;
}UsciAUartCtl;

/* -------------------------------- +
|	Function						|
+ ---------------------------------*/
void UsciAUartLibInit(void)
{
	uint8_t i= 0;

	// -- Reset all module -- //
	for (;i<USCIAUART_MODULE_NB;i++)
	{
		UsciAUartCtl.module[i] = NULL;
		UsciAUartCtl.moduleState[i] = stateInit;
	}
	// ---------------------- //
	UsciAUartCtl.moduleInitNb = 0;
}

UsciAUartHandle_t * UsciAUartCreate(uint16_t baseAddress, uint16_t bufferSize)
{
	uint8_t i=0, flag=0;
	int8_t	idAvailable=-1;


	// -- Sanity check -- //
	if (UsciAUartCtl.moduleInitNb < USCIAUART_MODULE_NB)
	{
		// -- Check all the activated module -- //
		for (i=0; i<USCIAUART_MODULE_NB; i++)
		{
			if (UsciAUartCtl.moduleState[i] == stateActive)
			{
				if (UsciAUartCtl.module[i]->baseAddress == baseAddress)
					return NULL;				//Module already activated by an other user
			}
			else
				idAvailable = (uint8_t)i;		//Found a space in the array for the new module
		}
		if (idAvailable < 0)
			return NULL;						//No space found (should never happen)
		// ------------------------------------ //

		for (i=0; i<USCIAUART_MODULE_NB; i++)
		{
			if (baseAddress == _UsciAUartValidBaseAddress[i])
				flag = 1;	//Valid BaseAddress found
		}

		if (!flag)
			return NULL;	//Invalid baseAddress given
	// ------------------ //

		UsciAUartHandle_t * tempPtr;

		// -- Allocate module handle -- //
		tempPtr = (UsciAUartHandle_t*)malloc(sizeof(UsciAUartHandle_t));
		if (tempPtr == NULL)
			return NULL;
		// ---------------------------- //

		// -- Allocate buffers -- //
		tempPtr->rxBuffer = rBufCreate(bufferSize);
		if (tempPtr->rxBuffer == NULL)
		{
			free(tempPtr);
			return NULL;
		}
		tempPtr->txBuffer = rBufCreate(bufferSize);
		if (tempPtr->rxBuffer == NULL)
		{
			rBufDestroy(tempPtr->rxBuffer);
			free(tempPtr);
			return NULL;
		}
		// ---------------------- //

		// -- Save information -- //
		HWREG8(baseAddress + OFS_UCAxCTL1) |= UCSWRST;		//Disable the USCI
		tempPtr->baseAddress = baseAddress;
		tempPtr->state = stateIdle;
		UsciAUartCtl.module[idAvailable] = tempPtr;
		UsciAUartCtl.moduleState[idAvailable] = stateActive;
		UsciAUartCtl.moduleInitNb++;
		// ---------------------- //

		return tempPtr;
	}
	return NULL;
}

stdError_t UsciAUartDestroy(UsciAUartHandle_t * moduleHandle)
{
	uint8_t	i=0;
	
	// -- Find the module -- //
	for (; i<USCIAUART_MODULE_NB; i++)
	{
		if (moduleHandle == UsciAUartCtl.module[i])
		{
	// --------------------- //
			HWREG8(moduleHandle->baseAddress + OFS_UCAxCTL1) |= UCSWRST;		//Disable the USCI
			// -- Destroy the buffers -- //
			rBufDestroy(moduleHandle->rxBuffer);
			rBufDestroy(moduleHandle->txBuffer);
			// ------------------------- //

			// -- Destroy the handle -- //
			free(moduleHandle);
			// ------------------------ //

			// -- Reinit the information -- //
			UsciAUartCtl.module[i] = NULL;
			UsciAUartCtl.moduleState[i] = stateInit;
			UsciAUartCtl.moduleInitNb--;
			// ---------------------------- //

			return errSuccess;
		}
	}
	return errBadAddress;
}

stdError_t UsciAUartConfigure(UsciAUartHandle_t * moduleHandle, uint8_t UCAxCTL0, uint8_t UCAxCTL1, uint16_t UCAxBRW, uint8_t UCAxMCTL)
{
	if (moduleHandle <= NULL)
		return errNoDevice;

	uint8_t	i=0;
	// -- Find the module -- //
	for (; i<USCIAUART_MODULE_NB; i++)
	{
		if (moduleHandle == UsciAUartCtl.module[i])
	// --------------------- //
		{
			if (UsciAUartCtl.moduleState[i] != stateActive)
				return errNotReady;

			// -- Configure the USCI -- //
			HWREG8(moduleHandle->baseAddress + OFS_UCAxCTL1) |= UCSWRST;		//Disable the USCI

			HWREG8(moduleHandle->baseAddress + OFS_UCAxCTL0) = (UCAxCTL0 & _USCIAUART_UCAxCTL0_MASK);			//Force Sync mode OFF
			HWREG8(moduleHandle->baseAddress + OFS_UCAxCTL1) = (UCAxCTL1 & _USCIAUART_UCAxCTL1_MASK) | UCSWRST;	//Keep the reset active
			HWREG16(moduleHandle->baseAddress + OFS_UCAxBRW) = UCAxBRW;
			HWREG8(moduleHandle->baseAddress + OFS_UCAxMCTL) = UCAxMCTL;

			HWREG8(moduleHandle->baseAddress + OFS_UCAxCTL1) &= ~(UCSWRST);		//Enable the USCI
			HWREG8(moduleHandle->baseAddress + OFS_UCAxIE) = (UCRXIE+UCTXIE);
			// ----------------------- //

			return errSuccess;
		}
	}
	return errNoDevice;
}

stdError_t UsciAUartBufferResize(UsciAUartHandle_t * moduleHandle, uint16_t newSize)
{
	if (moduleHandle <= NULL)
		return errNoDevice;

	stdError_t ec;
	ec = rBufResize(moduleHandle->rxBuffer, newSize);
	if ( ec == errSuccess)
		ec = rBufResize(moduleHandle->txBuffer, newSize);
	return ec;
}

void UsciAUartBufferFlush(UsciAUartHandle_t * moduleHandle)
{
	rBufFlush(moduleHandle->txBuffer);
	rBufFlush(moduleHandle->rxBuffer);
}

uint16_t UsciAUartRxPendingByteGet(UsciAUartHandle_t * moduleHandle)
{
	return rBufUsedSpaceGet(moduleHandle->rxBuffer);
}

uint16_t UsciAUartSend(UsciAUartHandle_t * moduleHandle, uint8_t * srcPtr, uint16_t byteNb)
{
	if (srcPtr <= NULL)
		return 0;

	if (moduleHandle <= NULL)
		return 0;

	// Save in txBuffer
	DBGPRINT((uint8_t)byteNb);
	DBGSET(BIT0);
	uint16_t byteBuffed = rBufArrayPush(moduleHandle->txBuffer, srcPtr, byteNb);
	DBGCLR(BIT0);
	DBGPRINT((uint8_t)byteBuffed);

	// Activate the USCI
	if ((moduleHandle->state == stateIdle) && (byteBuffed))
	{
		moduleHandle->state = stateActive;
		//TODO: check if HW flag could be use instead
		//Disable interrupt because we would not return from rBufBytePull before the first byte was sent
		uint16_t intState = __get_interrupt_state();
		__disable_interrupt();
		DBGSET(BIT1);
		rBufBytePull(moduleHandle->txBuffer,(uint8_t *)(moduleHandle->baseAddress+OFS_UCAxTXBUF));	//Pull from the buffer to keep correct order
		//Restore interrupt state
		DBGCLR(BIT1);
		__set_interrupt_state(intState);
		// ignore return value for now...
	}

	return byteBuffed;
}

uint16_t UsciAUartReceive(UsciAUartHandle_t * moduleHandle, uint8_t * dstPtr, uint16_t byteNb)
{
	if (dstPtr == NULL)
		return 0;

	if (moduleHandle <= NULL)
		return 0;

	// Pull from rxBuffer
	return rBufArrayPull(moduleHandle->rxBuffer, dstPtr, byteNb);
}

void UsciAUartISR(UsciAUartHandle_t * moduleHandle)
{
	uint16_t baseAddress = moduleHandle->baseAddress;

	switch(__even_in_range(HWREG16(baseAddress+OFS_UCAxIV), _USCIAUART_TX_INT))
	{
		// new byte received
		case _USCIAUART_RX_INT:
		{
			//Buff the received byte
			if(!rBufBytePush(moduleHandle->rxBuffer, (uint8_t *)(baseAddress+OFS_UCAxRXBUF)))
			{
				__no_operation();		//Byte was not transfered, handle accordingly
			}
			//Check register for errors and handle them

			//Auto-clear flag
			break;
		}
		// Ready to send a new byte
		case _USCIAUART_TX_INT:
		{
			if (moduleHandle->state == stateActive)
			{
				if (UsciAUartTxPendingByteGet(moduleHandle))
				{
					if (!rBufBytePull(moduleHandle->txBuffer, (uint8_t *)(baseAddress+OFS_UCAxTXBUF)))
					{
						__no_operation();
					}
					//Auto-clear flag
				}
				else
				{
					moduleHandle->state = stateIdle;			//Send finished
					HWREG8(baseAddress+OFS_UCAxIFG) = 0;
				}
			}
			else
			{
				moduleHandle->state = stateIdle;
				HWREG8(baseAddress+OFS_UCAxIFG) = 0;
			}
			break;
		}
		default:
		{
			moduleHandle->state = stateIdle;
			HWREG8(baseAddress+OFS_UCAxIFG) = 0;
			break;
		}
	}
}
