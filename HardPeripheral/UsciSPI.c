/*
 * UsciSPI.c
 *
 *	Created on: 2015-04-11
 *	Author: Laurence DV
 */

#include "UsciSPI.h"
/* -------------------------------- +
|	Global Variable					|
+ ---------------------------------*/
const uint16_t _UsciSPIValidBaseAddress[USCISPI_MODULE_NB] = {USCI_A0_BASE, USCI_A1_BASE, USCI_B0_BASE, USCI_B1_BASE};

struct{
	stdState_t			moduleState[USCISPI_MODULE_NB];
	UsciSPIModule_t * 	module[USCISPI_MODULE_NB];
	uint8_t				moduleInitNb;
	uint16_t			transferIDcnt;
}UsciSPICtl;

uint8_t dump;

extern uint16_t heapAvailable;

/* -------------------------------- +
|	Function						|
+ ---------------------------------*/
// ====== Internal function ====== //
void __UsciSPIDoTransfer(UsciSPIModule_t * moduleHandle, UsciSPITransfer_t * transfer)
{
	*(transfer->stateFlag) = stateActive;
	DBGSET(BIT0);
	moduleHandle->current.transfer = *transfer;
	DBGCLR(BIT0);
	moduleHandle->current.byteTx = 1;
	moduleHandle->current.byteRx = 0;

	moduleHandle->state = stateActive;

	// -- Select SS -- //
	*(transfer->slaveHandle->SSPort) &= ~(transfer->slaveHandle->SSpinMask);
	// --------------- //

	HWREG8(moduleHandle->baseAddress + OFS_UCAxTXBUF__SPI) = transfer->txBuffer[0];	//Push the first byte to start the hardware
	HWREG8(moduleHandle->baseAddress + OFS_UCAxIE__SPI) = (UCRXIE+UCTXIE);
}


// ==== Management Function ==== //
void UsciSPILibInit(void)
{
	uint8_t i=0;

	// -- Reset all module -- //
	for (;i<USCISPI_MODULE_NB;i++)
	{
		UsciSPICtl.module[i] = NULL;
		UsciSPICtl.moduleState[i] = stateInit;
	}
	// ---------------------- //
	UsciSPICtl.moduleInitNb = 0;
	UsciSPICtl.transferIDcnt = _USCISPI_TRANSFERID_CNT_INIT;
}

UsciSPIModule_t * UsciSPICreate(uint16_t baseAddress, uint8_t transferQueueSize)
{
	if (baseAddress <= 0)
		return NULL;

	if (transferQueueSize == 0)
		transferQueueSize = _USCISPI_DEFAULT_TRANSFERQUEUE_SIZE;

		UsciSPIModule_t * tempPtr;

	// -- Allocate the module -- //
	tempPtr = (UsciSPIModule_t *)malloc(sizeof(UsciSPIModule_t));
	if (tempPtr <= NULL)
		return NULL;
	// ------------------------- //

	// -- Allocate the transfer queue -- //
	tempPtr->transferAbortID = (uint16_t**)malloc(sizeof(uint16_t) * transferQueueSize);
	if (tempPtr->transferAbortID == NULL)
	{
		free(tempPtr);
		return NULL;
	}

	tempPtr->transferQueue = rBufCreate(sizeof(UsciSPITransfer_t) * transferQueueSize);
	if (tempPtr->transferQueue == NULL)
	{
		free(tempPtr->transferAbortID);
		free(tempPtr);
		return NULL;
	}
	// --------------------------------- //

	// -- Save information -- //
	tempPtr->baseAddress = baseAddress;
	tempPtr->transferAbortNb = 0;
	tempPtr->state = stateIdle;
	UsciSPICtl.module[UsciSPICtl.moduleInitNb] = tempPtr;				//TODO: refactor this mechanism...
	UsciSPICtl.moduleState[UsciSPICtl.moduleInitNb] = stateActive;		//TODO: refactor this mechanism...
	UsciSPICtl.moduleInitNb++;											//TODO: refactor this mechanism...
	// ---------------------- //

	return tempPtr;
}

void UsciSPIDestroy(UsciSPIModule_t * moduleHandle)
{
	if (moduleHandle <= NULL)
		return;
	//TODO
	// Find any pending transfer for this module
	// Stop everything

	rBufDestroy(moduleHandle->transferQueue);
	free(moduleHandle);
}

stdError_t UsciSPIConfigure(UsciSPIModule_t * moduleHandle, uint8_t UCxxCTL0Val, uint8_t UCxxCTL1Val, uint16_t UCxxBRxVal)
{
	if (moduleHandle <= NULL)
		return errNoDevice;

	// -- configure the USCI -- //
	HWREG8(moduleHandle->baseAddress + OFS_UCAxCTL1__SPI) |= UCSWRST;				//Disable the USCI
	HWREG8(moduleHandle->baseAddress + OFS_UCAxCTL0__SPI) = (UCxxCTL0Val & _USCISPI_UCxxCTL0_MASK) | UCSYNC;		//Force Sync mode ON
	HWREG8(moduleHandle->baseAddress + OFS_UCAxCTL1__SPI) = (UCxxCTL1Val & _USCISPI_UCxxCTL1_MASK) | UCSWRST;		//Keep the reset active
	HWREG16(moduleHandle->baseAddress + OFS_UCAxBRW__SPI) = UCxxBRxVal;
	HWREG8(moduleHandle->baseAddress + OFS_UCAxMCTL__SPI) = 0;

	//configure UCxxSTAT for listening mode here
	HWREG8(moduleHandle->baseAddress + OFS_UCAxCTL1__SPI) &= ~UCSWRST;				//Enable the USCI
	// ------------------------ //

	return errSuccess;
}

void UsciSPIEngine(UsciSPIModule_t * moduleHandle)
{
	//TODO: only if really needed
}


// ==== Master Function ==== //
UsciSPISlave_t * UsciSPISlaveCreate(UsciSPIModule_t * moduleHandle, volatile uint8_t * SSPort, uint8_t SSpinMask)
{
	#ifdef USCISPI_SANITY_CHECK_ENABLE
		if (moduleHandle <= NULL)
			return NULL;

		if (SSPort <= NULL)
			return NULL;
	#endif

	UsciSPISlave_t * tempPtr;

	// -- Allocate memory -- //
	tempPtr = (UsciSPISlave_t*)malloc(sizeof(UsciSPISlave_t));
	if (tempPtr != NULL)
	{
		// -- Init -- //
		tempPtr->module = moduleHandle;
		tempPtr->SSPort = SSPort;
		tempPtr->SSpinMask = SSpinMask;
		tempPtr->state = stateReady;
		// ---------- //
	}
	// --------------------- //

	return tempPtr;
}

void UsciSPISlaveDestroy(UsciSPISlave_t * slaveHandle)
{
	if (slaveHandle <= NULL)
		return;
	//TODO

	// -- Find any pending transfer -- //
	// ------------------------------- //

	free(slaveHandle);
}

// ==== Transfer Function ==== //
uint16_t UsciSPITransferStart(UsciSPISlave_t * slaveHandle, uint16_t byteNb, uint8_t * rxBuffer, uint8_t * txBuffer, stdState_t * stateFlag, UsciSPITransferMode_t transferMode)
{
	#ifdef USCISPI_SANITY_CHECK_ENABLE
		if (slaveHandle <= NULL)
			return 0;

		if (rxBuffer <= NULL)
			return 0;

		if (txBuffer <= NULL)
			return 0;

		if (stateFlag <= NULL)
			return 0;
	#endif

	if (byteNb > 0)
	{
		UsciSPIModule_t * moduleHandle = slaveHandle->module;

		if (rBufBufferIsFull(moduleHandle->transferQueue))
			return 0;

		UsciSPITransfer_t newTransfer;

		// -- Save information -- //
		newTransfer.slaveHandle = slaveHandle;
		newTransfer.byteNb = byteNb;
		newTransfer.rxBuffer = rxBuffer;
		newTransfer.txBuffer = txBuffer;
		newTransfer.stateFlag = stateFlag;
		newTransfer.mode = transferMode;
		newTransfer.transferID = UsciSPICtl.transferIDcnt;
		// ---------------------- //

		if (UsciSPICtl.transferIDcnt >= _USCISPI_TRANSFERID_CNT_ROLLOVER)
			UsciSPICtl.transferIDcnt = _USCISPI_TRANSFERID_CNT_INIT;
		else
			UsciSPICtl.transferIDcnt++;

		// -- Start immediately ---- //
		if (moduleHandle->state == stateIdle)
		{
			__UsciSPIDoTransfer(moduleHandle, &newTransfer);
//			*stateFlag = stateActive;
//			moduleHandle->current.transfer = newTransfer;
//			moduleHandle->current.byteTx = 1;
//			moduleHandle->current.byteRx = 0;
//
//			moduleHandle->state = stateActive;
//
//			// -- Select SS -- //
//			*(slaveHandle->SSPort) &= ~(slaveHandle->SSpinMask);
//			// --------------- //
//
//			HWREG8(moduleHandle->baseAddress + OFS_UCAxTXBUF__SPI) = txBuffer[0];	//Push the first byte to start the hardware
//			HWREG8(moduleHandle->baseAddress + OFS_UCAxIE__SPI) = (UCRXIE+UCTXIE);

		}
		// -- Append to the queue -- //
		else
		{
			*stateFlag = statePending;
			rBufArrayPush(moduleHandle->transferQueue, (uint8_t*)&newTransfer, sizeof(UsciSPITransfer_t));		//THIS could crap, newTransfer is on the stack....
		}
		// ------------------------- //

		return newTransfer.transferID;
	}

	return 0;
}

stdError_t UsciSPITransferAbort(uint16_t transferID)
{
	if (transferID != 0)			//ID 0 is invalid so already aborted
	{

	}

	return errSuccess;
}


// ==== Interrupt Service Routine ==== //
void UsciSPIISR(UsciSPIModule_t * moduleHandle)
{
	uint16_t baseAddress = moduleHandle->baseAddress;

	__no_operation();

	if (HWREG8(baseAddress + OFS_UCAxIFG__SPI) & _USCISPI_RX_INT_MASK)
	{

		// -- Send byte to the designated buffer -- //
		(moduleHandle->current.transfer.rxBuffer)[(moduleHandle->current.byteRx)] = HWREG8(baseAddress + OFS_UCAxRXBUF__SPI);
		(moduleHandle->current.byteRx)++;
		// ---------------------------------------- //

		if ((moduleHandle->current.byteRx) >= (moduleHandle->current.transfer.byteNb))
		{
			// De-select SS
			*(moduleHandle->current.transfer.slaveHandle->SSPort) |= (moduleHandle->current.transfer.slaveHandle->SSpinMask);

			*(moduleHandle->current.transfer.stateFlag) = stateDone;

			// -- Load the next transfer -- //
			if (rBufBufferIsEmpty(moduleHandle->transferQueue))
				moduleHandle->state = stateIdle;
			else
			{
//				moduleHandle->current.transfer = 0;

			}
			// ---------------------------- //
		}
	}

	if (HWREG8(baseAddress + OFS_UCAxIFG__SPI) & _USCISPI_TX_INT_MASK)
	{
		// -- Check if the transfer is finished -- //
		if ((moduleHandle->current.byteTx) >= (moduleHandle->current.transfer.byteNb))
		{
			HWREG8(moduleHandle->baseAddress + OFS_UCAxIE__SPI) &= ~UCTXIE;
		}
		// --------------------------------------- //
		else
		{
			if (moduleHandle->current.transfer.mode.toggleSS)
			{
				uint16_t i;
				while(HWREG8(baseAddress + OFS_UCAxSTAT) & UCBUSY);		//wait for the transmission to be finished

				*(moduleHandle->current.transfer.slaveHandle->SSPort) |= (moduleHandle->current.transfer.slaveHandle->SSpinMask);
				for (i=0; i < (moduleHandle->current.transfer.mode.toggleDelay); i++);
				*(moduleHandle->current.transfer.slaveHandle->SSPort) &= ~(moduleHandle->current.transfer.slaveHandle->SSpinMask);
			}

			//Send another byte
			HWREG8(baseAddress + OFS_UCAxTXBUF__SPI) = (moduleHandle->current.transfer.txBuffer)[(moduleHandle->current.byteTx)];
			moduleHandle->current.byteTx++;
		}
	}
}
