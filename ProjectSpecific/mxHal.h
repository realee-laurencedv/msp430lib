/*
 * mxHal.h
 *
 *	Created on: Mar 21, 2015
 *	Author: 	Laurence DV
 *	Note:		Project specific definition
 *
 */

#ifndef MXHAL_H_
#define MXHAL_H_

/*--------------------------------- +
|	Include							|
+ ---------------------------------*/
#include <stdint.h>
#include <driverlib.h>

/*--------------------------------- +
|	Define							|
+ ---------------------------------*/
// ==== Clock ==== //
#define CLOCK_SYSTEM_HZ			25000000			//This is the initial system clock frequency (driving the CPU)
#define XTAL_FREQ_XT1_HZ		0					//Frequency of the crystal/resonator connected to XT1
#define XTAL_FREQ_XT2_HZ		4000000				//Frequency of the crystal/resonator connected to XT2
// =============== //

// ==== USB ==== //
#define USB_INTF_ID			CDC0_INTFNUM			//useful abstraction define for USB functionnality (not included in MSP430lib, check: http://www.ti.com/tool/MSP430USBDEVPACK)
// ============= //

// ==== IO Map ==== //
#define GPIO_ALL	(	GPIO_PIN0|	GPIO_PIN1|	GPIO_PIN2|	GPIO_PIN3	|\
						GPIO_PIN4|	GPIO_PIN5|	GPIO_PIN6|	GPIO_PIN7	)

// Port 1
#define IOM_LED_GREEN_PIN		GPIO_PIN0
#define IOM_LED_GREEN_PORT		GPIO_PORT_P1
#define IOM_BTN1_PIN			GPIO_PIN1
#define IOM_BTN1_PORT			GPIO_PORT_P1
#define IOM_LED_RGB_R_PIN		GPIO_PIN5
#define IOM_LED_RGB_R_PORT		GPIO_PORT_P1
#define IOM_LED_RGB_G_PIN		GPIO_PIN4
#define IOM_LED_RGB_G_PORT		GPIO_PORT_P1
#define IOM_LED_RGB_B_PIN		GPIO_PIN3
#define IOM_LED_RGB_B_PORT		GPIO_PORT_P1
#define IOM_SPI_CS3_PIN			GPIO_PIN2
#define IOM_SPI_CS3_PORT		GPIO_PORT_P1
#define IOM_P1_UNUSED_PIN		(	GPIO_PIN6|	GPIO_PIN7	)

// Port 2
#define IOM_BTN2_PIN			GPIO_PIN1
#define IOM_BTN2_PORT			GPIO_PORT_P2
#define IOM_TEMP_IRQ_PIN		GPIO_PIN7
#define IOM_TEMP_IRQ_PORT		GPIO_PORT_P2
#define IOM_L6470_FLAG_PIN		GPIO_PIN6
#define IOM_L6470_FLAG_PORT		GPIO_PORT_P2
#define IOM_L6470_BUSY_PIN		GPIO_PIN3
#define IOM_L6470_BUSY_PORT		GPIO_PORT_P2
#define IOM_L6470_STCK_PIN		GPIO_PIN5
#define IOM_L6470_STCK_PORT		GPIO_PORT_P2
#define IOM_SPI_CS4_PIN			GPIO_PIN4
#define IOM_SPI_CS4_PORT		GPIO_PORT_P2
#define IOM_P2_UNUSED_PIN		(	GPIO_PIN0|	GPIO_PIN2)

// Port 3
#define IOM_UART_RX_PIN			GPIO_PIN4
#define IOM_UART_RX_PORT		GPIO_PORT_P3
#define IOM_UART_TX_PIN			GPIO_PIN3
#define IOM_UART_TX_PORT		GPIO_PORT_P3
#define IOM_SPI_SCK_PIN			GPIO_PIN2
#define IOM_SPI_SCK_PORT		GPIO_PORT_P3
#define IOM_SPI_MOSI_PIN		GPIO_PIN0
#define IOM_SPI_MOSI_PORT		GPIO_PORT_P3
#define IOM_SPI_MISO_PIN		GPIO_PIN1
#define IOM_SPI_MISO_PORT		GPIO_PORT_P3
#define IOM_SPI_CS2_PIN			GPIO_PIN7
#define IOM_SPI_CS2_PORT		GPIO_PORT_P3
#define IOM_P3_UNUSED_PIN		(	GPIO_PIN5|	GPIO_PIN6)

// Port 4
#define IOM_LED_RED_PIN			GPIO_PIN7
#define IOM_LED_RED_PORT		GPIO_PORT_P4
#define IOM_UART_TX_EN_PIN		GPIO_PIN0
#define IOM_UART_TX_EN_PORT		GPIO_PORT_P4
#define IOM_UART_RX_EN_PIN		GPIO_PIN3
#define IOM_UART_RX_EN_PORT		GPIO_PORT_P4
#define IOM_I2C_SDA_PIN			GPIO_PIN1
#define IOM_I2C_SDA_PORT		GPIO_PORT_P4
#define IOM_I2C_SCL_PIN			GPIO_PIN2
#define IOM_I2C_SCL_PORT		GPIO_PORT_P4
#define IOM_P4_UNUSED_PIN		(	GPIO_PIN4|	GPIO_PIN5|	GPIO_PIN6)

// Port 5
#define IOM_XT2IN_PIN			GPIO_PIN2
#define IOM_XT2IN_PORT			GPIO_PORT_P5
#define IOM_XT2OUT_PIN			GPIO_PIN3
#define IOM_XT2OUT_PORT			GPIO_PORT_P5
#define IOM_P5_UNUSED_PIN		(	GPIO_PIN0|	GPIO_PIN1|							\
									GPIO_PIN4|	GPIO_PIN5|	GPIO_PIN6|	GPIO_PIN7	)

// Port 6
#define IOM_P6_UNUSED_PIN		GPIO_ALL

// Port 7
#define IOM_P7_UNUSED_PIN		GPIO_ALL

// Port 8
#define IOM_SPI_CS1_PIN			GPIO_PIN1
#define IOM_SPI_CS1_PORT		GPIO_PORT_P8
#define IOM_L6470_RESET_PIN		GPIO_PIN2
#define IOM_L6470_RESET_PORT	GPIO_PORT_P8
#define IOM_P8_UNUSED_PIN		(	GPIO_PIN0|							GPIO_PIN3	|\
									GPIO_PIN4|	GPIO_PIN5|	GPIO_PIN6|	GPIO_PIN7	)

// Port 9
#define IOM_P9_UNUSED_PIN		GPIO_ALL

// Port J
#define IOM_PJ_UNUSED_PIN		GPIO_ALL

// ================ //

// ==== Peripheral Map ==== //
#define PEM_BTN1_VECTOR			PORT1_VECTOR
#define PEM_BTN1_INTMASK		BIT1

#define PEM_BTN2_VECTOR			PORT2_VECTOR
#define PEM_BTN2_INTMASK		BIT1

#define PEM_PWM_TIMER			TIMER_A0_BASE
#define PEM_PWM_VECTOR			TIMER0_A0_VECTOR
// ======================== //

/*--------------------------------- +
|	Type							|
+ ---------------------------------*/

/*--------------------------------- +
|	Prototype						|
+ ---------------------------------*/
// Project Init function
void init(void);

#endif /* MXHAL_H_ */
