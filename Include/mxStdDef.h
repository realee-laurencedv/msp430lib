/*
 * mxStdDef.h
 *
 *	Created on: 2015-02-26
 *	Author: 	Laurence DV
 *	Note:		Standard definition used by many lib
 *				Require Run-Time Model Option: --small-enum
 */

#ifndef MXSTDDEF_H_
#define MXSTDDEF_H_

#include <stddef.h>
#include <stdint.h>

/* -------------------------------- +
|	Type							|
+ ---------------------------------*/
typedef enum {
	// Generic Error
	errSuccess = 0,
	errFail = 1,
	errInterrupt = 4,
	errIO = 5,
	errTooBig = 7,
	errFormat = 8,
	errInvalidArg = 22,
	errTryAgain = 11,
	errNoMem = 12,
	errNoPermission = 13,
	errBadAddress = 14,
	errBusy = 16,
	errNoDevice = 19,

	// File Error
	errNoFile = 2,
	errBadFile = 9,
	errTableOverflow = 23,
	errTooMany = 24,
	errFileBusy = 26,
	errFileTooBig = 27,
	errNoSpace = 28,
	errFileIllegalSeek = 29,
	errReadOnly = 30,

	// Math Error
	errMathDomain = 33,
	errMathRange = 34,

	// Other Error
	errNotReady = 64,
	errChecksum = 65,
	errNotFound = 66
}stdError_t;

typedef enum {
	stateInit = 0,
	stateIdle = 1,
	stateActive = 2,
	stateExit = 3,
	stateReport = 4,
	stateError = 5,
	stateRetry = 6,
	stateSend = 7,
	stateReceive = 8,
	stateTransmit = 9,
	stateBypass = 10,
	stateReady = 11,
	stateProbe = 12,
	stateWork = 13,
	stateProgram = 14,
	statePending = 15,
	stateWait = 16,			//Should be only used for signaling that work is still underway
	stateDone = 17,			//Should be only used for signaling everything is finished, ex: step finished go to next step
	stateAbort = 18
}stdState_t;

#endif /* MXSTDDEF_H_ */
