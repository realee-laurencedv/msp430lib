MSP430 libs
==========
Those are peripherals, soft-peripherals and specefic devices C librairies for MSP430 µC.
They are tested on MSP430F5xxx family, but should be compatible with other family using the same peripherals.
The Softperipheral libs are almost directly portable, check the headers to know exactly what is device specific.
All lib uses interrupt when they are relevant but do not control the global interrupt, this must be done in the main.
The Interrupt vector are also not provided, they must be defined external to the lib files, I suggest in the main.c.
An example is provided in the *ProjectSpecific* folder. Most of my lib work with *engine*  but some are service provider.

**NOTE:** I depend on the [TI's driverlib](www.ti.com/tool/mspdriverlib) for hardware definition and some lib, I strongly recommend that you include them in your project too. You may also want [TI's USB package](www.ti.com/tool/MSP430USBDEVPACK) if you are doing a project with the embbeded USB of the MSP430F5xxx or F6xxx series.

### Engine
The *engine* are special function that you need to put in the main loop. Some will need to be executed at a maximum interval, but they are mostly responding to interrupt and other event, so the real need will come from your usage. A good example would be the USCI A as Uart.

### Service
Serving providing lib are simple lib that provide a specific service and are executed in the space of the application code executing them. A good example would be the ring-buffer.

## HardPeripheral
* USCI A as UART

## SoftPeripheral
* Ring-Buffer

## Device
* None (for now)

## Utils
* Curve tracing (integer)

## Include
Standard definition and often used types and macro

## ProjectSpecific
This folder is populated with template file needed for certain lib, you should copy them in the root folder of your project and adjust the project specific variables, function and defines.