/*
 * RingBuffer.c
 *
 *  Created on: 2015-02-20
 *      Author: Laurence DV
 */

#include "SoftPeripheral/RingBuffer.h"

/* -------------------------------- +
|	Global Variable					|
+ ---------------------------------*/


/* -------------------------------- +
|	Function						|
+ ---------------------------------*/
// Management functions
rBuf_t * rBufCreate(uint16_t size)
{
	// Check if the size is valid
	if(size == 0)
		return NULL;
	else
	{
		rBuf_t * tempPtr;

		// -- Allocate the control -- //
		tempPtr = (rBuf_t*)malloc(sizeof(rBuf_t));
		if (tempPtr == NULL)
			return NULL;
		// -------------------------- //

		// -- Allocate the buffer -- //
		tempPtr->buffer = (uint8_t*)malloc(size);
		if (tempPtr->buffer == NULL)
		{
			free(tempPtr);
			return NULL;
		}
		// ------------------------- //

		// -- Init the buffer -- //
		tempPtr->bufferSize = size;
		tempPtr->elementNb = 0;
		tempPtr->in = tempPtr->buffer;
		tempPtr->out = tempPtr->buffer;
		tempPtr->end = (tempPtr->buffer)+size-1;
		// --------------------- //

		return tempPtr;
	}
}

void rBufDestroy(rBuf_t * rBuf)
{
	if (rBuf <= NULL)
		return;
	else
	{
		// -- Desallocate the buffer -- //
		free((void *)(rBuf->buffer));
		// ---------------------------- //

		// -- Desallocate the control -- //
		free(rBuf);
		// ----------------------------- //
	}
}

//TODO: NOT WORKING AT ALL, need update to the "nolock" scheme
stdError_t rBufResize(rBuf_t * rBuf, uint16_t newSize)
{
	// Handle null pointer
	if (rBuf <= NULL)
		return errBadAddress;

	if (newSize == rBuf->elementNb)
		return errSuccess;

	if (newSize >= rBuf->elementNb)
	{
		// -- Lock the buffer -- //
		uint16_t intState;
		intState = __get_interrupt_state();
		__disable_interrupt();
		if (_RBUF_ANY_LOCK_IS_LOCKED(rBuf))
		{
			__set_interrupt_state(intState);
			return errBusy;
		}
		_RBUF_PUSH_LOCK(rBuf);
		_RBUF_PULL_LOCK(rBuf);
		__set_interrupt_state(intState);
		// --------------------- //

		// -- Create temp buf -- //
		uint16_t tempElementNb = rBuf->elementNb;
		uint8_t * tempBuffer;
		if (tempElementNb > 0)
		{
			tempBuffer = malloc(tempElementNb);
			if (tempBuffer == NULL)
				return errNoMem;
		// --------------------- //

			// -- Copy to temp -- //
			if (tempElementNb != rBufArrayPull(rBuf, tempBuffer, tempElementNb))
			{
				free(tempBuffer);
				return errFail;
			}
			// ------------------ //
		}

		// -- Create the new buff and fill it -- //
		rBuf->buffer = (uint8_t *)realloc((void *)(rBuf->buffer), newSize);
		if (rBuf->buffer == NULL)
		{
			free(tempBuffer);
			rBuf->buffer = (uint8_t *)malloc(rBuf->bufferSize);		//Realloc the old size
			if (rBuf->buffer == NULL)
				return errFail;
			return errNoMem;
		}
		// -- Configure the buffer -- //
		rBuf->bufferSize = newSize;
		rBuf->elementNb = 0;			//Should already be at 0
		rBuf->in = rBuf->buffer;
		rBuf->out = rBuf->buffer;
		rBuf->end = rBuf->buffer + newSize-1;
		// -------------------------- //
		if (tempElementNb > 0)
		{
			if (tempElementNb != rBufArrayPush(rBuf, tempBuffer, tempElementNb))
			{
				free(tempBuffer);
				return errFail;
			}
			free(tempBuffer);
		}
		// ------------------------------------- //

		_RBUF_PULL_UNLOCK(rBuf);
		_RBUF_PUSH_UNLOCK(rBuf);

		return errSuccess;
	}
	return errNoSpace;
}

void rBufFlush(rBuf_t * rBuf)
{
	uint16_t intState;
	intState = __get_interrupt_state();
	__disable_interrupt();

	// -- Flush the content -- //
	rBuf->elementNb = 0;
	rBuf->in = rBuf->buffer;
	rBuf->out = rBuf->buffer;
	// ----------------------- //

	__set_interrupt_state(intState);
}

// Space functions
uint16_t rBufFreeSpaceGet(rBuf_t * rBuf)
{
	return ((rBuf->bufferSize)-(rBuf->elementNb));
}

uint16_t rBufUsedSpaceGet(rBuf_t * rBuf)
{
	return ((rBuf)->elementNb);
}

uint16_t rBufTotalSpaceGet(rBuf_t * rBuf)
{
	return (rBuf->bufferSize);
}

bool rBufBufferIsFull(rBuf_t * rBuf)
{
	if (rBuf->elementNb == rBuf->bufferSize)
		return 1;
	else
		return 0;
}

bool rBufBufferIsEmpty(rBuf_t * rBuf)
{
	if (rBuf->elementNb == 0)
		return 1;
	else
		return 0;
}


// Data functions
uint8_t rBufBytePush(rBuf_t * rBuf, uint8_t * srcPtr)
{
	uint8_t bytePushed = 0;
	uint16_t intState;

	if (srcPtr > NULL)
	{
		intState = __get_interrupt_state();
		__disable_interrupt();

		 if ((rBuf->elementNb) < (rBuf->bufferSize))
		 {
			// Push the byte
			*(rBuf->in) = *srcPtr;

			// Handle boundaries
			if (rBuf->in == rBuf->end)
				rBuf->in = rBuf->buffer;
			else
				rBuf->in++;

			// Count the byte
			rBuf->elementNb++;
			bytePushed = 1;
		 }

		//Restore interrupt state
		__set_interrupt_state(intState);
	}
	return bytePushed;
}

uint8_t rBufBytePull(rBuf_t * rBuf, uint8_t * dstPtr)
{
	uint8_t bytePulled = 0;
	uint16_t intState;

	if (dstPtr > NULL)
	{
		intState = __get_interrupt_state();
		__disable_interrupt();

		if (rBuf->elementNb)
		{
			// Push the byte
			*dstPtr = *(rBuf->out);

			// Handle boundaries
			if (rBuf->out == rBuf->end)
				rBuf->out = rBuf->buffer;
			else
				rBuf->out++;

			// Count the byte
			bytePulled = 1;
			rBuf->elementNb--;
		}
		//Restore interrupt state
		__set_interrupt_state(intState);
	}
	return bytePulled;
}

uint16_t rBufArrayPush(rBuf_t * rBuf, uint8_t * srcPtr, uint16_t byteNb)
{
	uint16_t bytePushed = 0;

	for (; bytePushed < byteNb; bytePushed++)
	{
		if (!rBufBytePush(rBuf, &srcPtr[bytePushed]))
		{
			DBGPULSE(BIT1);
			return bytePushed;
		}
	}
	return bytePushed;
}

uint16_t rBufArrayPull(rBuf_t * rBuf, uint8_t * dstPtr, uint16_t byteNb)
{
	uint16_t bytePulled = 0;

	for (; bytePulled < byteNb; bytePulled++)
	{
		if (!rBufBytePull(rBuf, &dstPtr[bytePulled]))
		{
			DBGPULSE(BIT2);
			return bytePulled;
		}
	}
	return bytePulled;
}

uint16_t rBufArrayPushProtected(rBuf_t * rBuf, uint8_t * srcPtr, uint16_t byteNb)
{
	uint16_t intState;
	uint16_t returnVal;
	intState = __get_interrupt_state();
	__disable_interrupt();
	if (byteNb > (rBuf->bufferSize)-(rBuf->elementNb))
		returnVal = 0;
	else
		returnVal = rBufArrayPush(rBuf, srcPtr, byteNb);
	__set_interrupt_state(intState);
	return returnVal;
}

uint16_t rBufArrayPullProtected(rBuf_t * rBuf, uint8_t * dstPtr, uint16_t byteNb)
{
	uint16_t intState;
	uint16_t returnVal;
	intState = __get_interrupt_state();
	__disable_interrupt();
	if (byteNb > (rBuf->elementNb))
		returnVal = 0;
	else
		returnVal = rBufArrayPull(rBuf, dstPtr, byteNb);
	__set_interrupt_state(intState);
	return returnVal;
}
