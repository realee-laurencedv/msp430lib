/*
 * RingBuffer.h
 *
 *	Created on: 2015-02-20
 *	Author:		Laurence DV
 *	Depend:		mxHal.c project specific definition
 *	Note:		Ring Buffer implementation for various uses
 *				Buffer element are octet only and the total number is dynamic.		
 */

/*	TIMING ANALYSIS
 * 	Function		|	Octet Nb	|	Tot Time	|	Time/Octet
 * 	=================================================================
 *	rBufBytePush	|	1 Octet		|	2.32�s		|	2.32�s
 *	=================================================================
 *	rBufBytePull	|	1 Octet		|	2.16�s		|	2.16�s
 *	=================================================================
 *	rBufArrayPush	|	1 Octet		|	3.52�s		|	3.52�s
 *					|	2 Octet		|	4.83�s		|	2.42�s
 *					|	3 Octet		|	6.16�s		|	2.05�s
 *					|	4 Octet		|	7.32�s		|	1.83�s
 *					|	5 Octet		|	8.64�s		|	1.73�s
 *					|	10 Octet	|	14.82�s		|	1.48�s
 *					|	50 Octet	|	64.40�s		|	1.29�s
 *					|	100 Octet	|	126.3�s		|	1.26�s
 *	=================================================================
 *	rBufArrayPull	|	1 Octet		|	3.23�s		|	3.23�s
 *					|	2 Octet		|	4.55�s		|	2.28�s
 *					|	3 Octet		|	5.75�s		|	1.92�s
 *					|	4 Octet		|	6.80�s		|	1.70�s
 *					|	5 Octet		|	7.99�s		|	1.60�s
 *					|	10 Octet	|	13.60�s		|	1.36�s
 *					|	50 Octet	|	58.33�s		|	1.17�s
 *					|	100 Octet	|	114.3�s		|	1.14�s
 * All tested with a global source and destination and a buffer of 100 byte
 * with a MCLK of 25MHz valid for commit #dca3318170a25a653ed33e60d09213e28e74161f
 * */

#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_

/* -------------------------------- +
|	Include							|
+ ---------------------------------*/
#include <msp430.h>
#include <driverlib.h>
#include <mxHal.h>
#include <Include/mxStdDef.h>
#include <Util/lhf.h>
#include <stdlib.h>

/* -------------------------------- +
|	Define							|
+ ---------------------------------*/
//#define RBUF_SANITY_CHECK_ENABLE	1	//Define this to enable sanity check of almost all pointer and values (no check in ISR)

#define _RBUF_LOCKED		0xFF
#define _RBUF_UNLOCKED		0x00		//Those value need to be maskable for _RBUF_CHECK_xxx

/* -------------------------------- +
|	Type							|
+ ---------------------------------*/
typedef struct{
	volatile uint8_t * in;
	volatile uint8_t * out;
	volatile uint8_t * end;
	volatile uint8_t * buffer;
	uint8_t pushLock;
	uint8_t pullLock;
	volatile uint16_t elementNb;
	uint16_t bufferSize;
}rBuf_t;

/* -------------------------------- +
|	Internal Macro					|
+ ---------------------------------*/
#define _RBUF_PUSH_LOCK(rBuf)				((rBuf)->pushLock = _RBUF_LOCKED)
#define _RBUF_PUSH_UNLOCK(rBuf)				((rBuf)->pushLock = _RBUF_UNLOCKED)
#define _RBUF_PULL_LOCK(rBuf)				((rBuf)->pullLock = _RBUF_LOCKED)
#define _RBUF_PULL_UNLOCK(rBuf)				((rBuf)->pullLock = _RBUF_UNLOCKED)

#define _RBUF_PUSH_LOCK_IS_LOCKED(rBuf)		((rBuf)->pushLock != _RBUF_UNLOCKED)
#define _RBUF_PULL_LOCK_IS_LOCKED(rBuf)		((rBuf)->pullLock != _RBUF_UNLOCKED)
#define _RBUF_ANY_LOCK_IS_LOCKED(rBuf)		((((rBuf)->pushLock) | ((rBuf)->pullLock)) != _RBUF_UNLOCKED)

#define _RBUF_PUSH_LOCK_IS_UNLOCKED(rBuf)	(!(_RBUF_PUSH_LOCK_IS_LOCKED(rBuf)))
#define _RBUF_PULL_LOCK_IS_UNLOCKED(rBuf)	(!(_RBUF_PULL_LOCK_IS_LOCKED(rBuf)))
#define _RBUF_ANY_LOCK_IS_UNLOCKED(rBuf)	(!(_RBUF_ANY_LOCK_IS_LOCKED(rBuf)))

/* -------------------------------- +
|	Prototype						|
+ ---------------------------------*/
// Create and return a handle for a  new ring buffer of the desired size (in byte)
// Return NULL if there is not enough heap space
rBuf_t * rBufCreate(uint16_t size);

// Destroy and free the allocated memory for a previously allocated ring buffer
void rBufDestroy(rBuf_t * rBuf);

//	TODO: NOT WORKING AT ALL, need update to the "nolock" scheme
// Resize a ring buffer to a new size, will copy the current content
// Return	errSuccess	if successful
//			errNoSpace	if too much element present for the new size
//			errNoMem 	if there is not enough memory to make the copy
//			errFail		if the buffer was access while resizing
//			errBusy		if the buffer is currently locked by another user
stdError_t rBufResize(rBuf_t * rBuf, uint16_t newSize);

// Flush the entire content of the buffer (elementNb = 0)
void rBufFlush(rBuf_t * rBuf);

// Return the current space available in the buffer (in byte)
uint16_t rBufFreeSpaceGet(rBuf_t * rBuf);

// Return the current space used in the buffer (in byte)
uint16_t rBufUsedSpaceGet(rBuf_t * rBuf);

// Return the maximum space available in this buffer (in byte)
uint16_t rBufTotalSpaceGet(rBuf_t * rBuf);

// Return true if the buffer is full
bool rBufBufferIsFull(rBuf_t * rBuf);

// Return true if the buffer is empty
bool rBufBufferIsEmpty(rBuf_t * rBuf);

// Push a byte into the buffer
// Return the number of byte pushed (max 1)
uint8_t rBufBytePush(rBuf_t * rBuf, uint8_t * srcPtr);

// Pull a byte from the buffer
// Return the number of byte pulled (max 1)
uint8_t rBufBytePull(rBuf_t * rBuf, uint8_t * dstPtr);

// Push an array of $byteNb size to the buffer from the $srcPtr
// Return the number of byte pushed
uint16_t rBufArrayPush(rBuf_t * rBuf, uint8_t * srcPtr, uint16_t byteNb);

// Pull an array of $byteNb size from the buffer to the $dstPtr
// Return the number of byte pulled
uint16_t rBufArrayPull(rBuf_t * rBuf, uint8_t * dstPtr, uint16_t byteNb);

// Concurrent access protected version of the array functions
// Note:	Those function ensure that no less or no more than $byteNb bytes were pushed/pulled.
// 			Disable global interrupt before pushing/pulling and restore it after
uint16_t rBufArrayPushProtected(rBuf_t * rBuf, uint8_t * srcPtr, uint16_t byteNb);
uint16_t rBufArrayPullProtected(rBuf_t * rBuf, uint8_t * dstPtr, uint16_t byteNb);

#endif /* RINGBUFFER_H_ */
